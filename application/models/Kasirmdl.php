<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kasirmdl extends CI_Model
{

 function produk_fetch($q)
 {

    $query = $this->db->query('SELECT a.* , (SUM(b.masuk) - SUM(b.keluar)) as sisa_stok FROM produk a LEFT JOIN produk_transaksi b ON a.id=b.id_produk WHERE a.nama LIKE "%'.$q.'%" GROUP BY a.id ');
    
    if($query->num_rows() > 0)
    {
        foreach($query->result_array() as $row)
        {
            if($row['sisa_stok'] > 0){
                $output[] = array(
                    'nama'  => $row["nama"],
                    'id' => $row['id'],
                    'type' => 'produk'
                );
            }
        }
        echo json_encode($output);
    }
}

function customer_fetch($q){
    $this->db->like('nama', $q);
    $query = $this->db->get('pelanggan');
    
    if($query->num_rows() > 0)
    {
        foreach($query->result_array() as $row)
        {
            $output[] = array(
                'nama'  => $row["nama"],
                'id' => $row['id'],
                'type' => 'customer'
            );
        }
        echo json_encode($output);
    }
}
}


?>