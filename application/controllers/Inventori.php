<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventori extends CI_Controller {


	public function __construct()
	{
		parent::__construct();

		if(!isset($_SESSION['userid']))
		{
			redirect(base_url().'masuk','refresh');
		}
	}

	function pagination($search,$item_per_page, $current_page, $total_records, $total_pages, $link=''){
		$pagination = '';

		$mindate = '';
		$maxdate= '';
		$status = '';


		if($mindate ==''){
			$mindate='all';
		}
		if($maxdate ==''){
			$maxdate='all';
		}
		if($status ==''){
			$status='all';
		}




		if($total_pages >= 0 && $current_page <= $total_pages){

			$pagination .= '<ul class="pagination">';


			$right_links    = $current_page + 10;
			$previous       = $current_page - 1;
			$next           = $current_page + 1;
			$first_link     = true;

			if($current_page > 1){
				$previous_link = ($previous==0)?1:$previous;
      $pagination .= '<li class="first page-item"><a class="page-link" href="'.base_url().$link.'1" title="First">&laquo;</a></li>'; //
      $pagination .= '<li class=" page-item"><a class="page-link" href="'.base_url().$link.'/'.$previous_link.'" title="Previous">&lt;</a></li>';
      for($i = ($current_page-2); $i < $current_page; $i++){
      	if($i > 0){
      		$pagination .= '<li class="page-item"><a class="page-link" href="'.base_url().$link.'/'.$i.'" title="Page'.$i.'">'.$i.'</a></li>';
      	}
      }
      $first_link = false;
  }

  if($first_link){
  	$pagination .= '<li class="first active page-item"><a class="page-link">'.$current_page.'</a></li>';
  }elseif($current_page == $total_pages){
  	$pagination .= '<li class="last active page-item"><a class="page-link">'.$current_page.'</a></li>';
  }else{
  	$pagination .= '<li class="active page-item"><a class="page-link">'.$current_page.'</a></li>';
  }

  for($i = $current_page+1; $i < $right_links ; $i++){
  	if($i<=$total_pages){
  		$pagination .= '<li class="page-item"><a class="page-link" href="'.base_url().$link.'/'.$i.'" title="Page '.$i.'">'.$i.'</a></li>';
  	}
  }

  if($current_page < $total_pages){

	      $pagination .= '<li class="page-item"><a class="page-link" href="'.base_url().$link.'/'.$next.'" title="Next">&gt;</a></li>'; //
	      $pagination .= '<li class="last page-item"><a class="page-link" href="'.base_url().$link.'/'.$total_pages.'" title="Last">&raquo;</a></li>';
	  }

	  $pagination .= '</ul>';
	}
	return $pagination;
}

public function index()
{
	$userid = $_SESSION['userid'];
	$data['mercant'] = $this->db->query("SELECT a.*,b.nama as pemilik FROM `data_mercant` a LEFt JOIN data_pelanggan b ON b.id_mercant = a.id_mercant WHERE a.status=1 AND a.sales_id='".$userid."'")->result_array();
	$this->load->view('admin/header',null);
	$this->load->view('inventori/mercant',$data);
	$this->load->view('admin/footer');
}

public function supplier()
{
	
	$data['supplier'] = $this->db->get('supplier')->result_array();
	$this->load->view('admin/header',null);
	$this->load->view('inventori/supplier',$data);
	$this->load->view('admin/footer');
}

public function form_supplier($id='')
{
	
	if($id != '')
	{
		$data['id'] = $id;
		$data['supplier'] = $this->db->get_where('supplier', array('id' => $id))->row_array();
	}else{
		$data = '';
	}
	$this->load->view('admin/header',null);
	$this->load->view('inventori/form_supplier',$data);
	$this->load->view('admin/footer');
}

public function simpan_supplier()
{
	$data = $this->input->post();

	$get = $this->db->get_where('supplier', array('tlp' => $data['tlp']));

	if($get->num_rows() > 0){
		echo "Phone Number already exists";
		exit;
	}

	if(isset($data['status'])){
		$status = 1;
	}else{
		$status = 0;
	}

	$add['nama'] = $data['nama'];
	$add['nama_owner'] = $data['nama_owner'];
	$add['alamat'] = $data['alamat'];
	$add['kota'] = $data['kota'];
	$add['kode_pos'] = $data['kode_pos'];
	$add['tlp'] = $data['tlp'];
	$add['status'] = $status;
	if(isset($data['id'])){
		if($this->db->update('supplier',$add, array('id' => $data['id'])))
		{
			echo 1;
		}
	}else{
		if($this->db->insert('supplier',$add))
		{
			echo 1;
		}
	}
}

public function stok_masuk()
{
	
	$data['transaksi'] = $this->db->query("SELECT * FROM produk_transaksi WHERE status='in' GROUP BY no_nota ORDER BY id DESC")->result_array();


	$this->load->view('admin/header',null);
	$this->load->view('inventori/stok_masuk',$data);
	$this->load->view('admin/footer');
}

public function opname()
{
	$this->db->group_by('no_nota');
	$data['transaksi'] = $this->db->get_where('produk_transaksi',  array('status' => 'op'))->result_array();
	$this->load->view('admin/header',null);
	$this->load->view('inventori/stok_opname',$data);
	$this->load->view('admin/footer');
}

public function kartu_stok()
{

	$data['transaksi'] = $this->db->query("SELECT b.nama, c.nama_kategori, SUM(a.masuk) as masuk , SUM(a.keluar) as keluar FROM `produk_transaksi` a 
		LEFT JOIN produk b ON a.id_produk = b.id
		LEFT JOIN kategori c ON b.id_kategori = c.id_kategori
		WHERE uid='".$_SESSION['userid']."' GROUP BY a.id_produk")->result_array();
	$this->load->view('admin/header',null);
	$this->load->view('inventori/kartu_stok',$data);
	$this->load->view('admin/footer');
}


public function stok_keluar()
{
	//$this->db->group_by('no_nota');
	$data['transaksi'] = $this->db->get_where('produk_transaksi',  array('status' => 'out'))->result_array();
	$this->load->view('admin/header',null);
	$this->load->view('inventori/stok_keluar',$data);
	$this->load->view('admin/footer');
}



public function act_produk()
{
	$id = $this->input->post('id');

	$get = $this->db->get_where('produk', array('id' => $id))->row_array();
	if($get['status'] == 1)
	{
		$add['status'] = 0;
	}else{
		$add['status'] = 1;
	}

	if($this->db->update('produk',$add, array('id' => $id))){
		echo 1;
	}
}

public function act_supplier()
{
	$id = $this->input->post('id');

	$get = $this->db->get_where('supplier', array('id' => $id))->row_array();
	if($get['status'] == 1)
	{
		$add['status'] = 0;
	}else{
		$add['status'] = 1;
	}

	if($this->db->update('supplier',$add, array('id' => $id))){
		echo 1;
	}
}

public function form_stok_masuk()
{
	$data = '';
	$this->load->view('admin/header',null);
	$this->load->view('inventori/form_stok_masuk',$data);
	$this->load->view('admin/footer');
}

public function form_opname()
{
	$data = '';
	$this->load->view('admin/header',null);
	$this->load->view('inventori/form_opname',$data);
	$this->load->view('admin/footer');
}

public function form_stok_keluar()
{
	$data = '';
	$this->load->view('admin/header',null);
	$this->load->view('inventori/form_stok_keluar',$data);
	$this->load->view('admin/footer');
}

public function detail_produk()
{	
	$id = $this->input->post('value');
	$get = $this->db->get_where('produk', array('id' => $id));

	if ($get->num_rows() > 0){ 

		$hasil = $get->row_array();

		echo json_encode($hasil);
		
	}

}

public function cari_produk()
{	
	$id = $this->input->post('value');
	$get = $this->db->get_where('produk', array('kode' => $id));

	if ($get->num_rows() > 0){ 

		$hasil = $get->row_array();

		echo json_encode($hasil);
		
	}

}


public function detail_produk_op(){
	$id = $this->input->post('value');
	$get = $this->db->query('SELECT (SUM(a.masuk) - SUM(a.keluar)) as sisa_stok , c.nama_satuan FROM produk_transaksi a 
		LEFT JOIN produk b ON a.id_produk = b.id
		LEFT JOIN t_unit c ON b.satuan = c.id WHERE a.id_produk='.$id);

	if ($get->num_rows() > 0) {

		$hasil = $get->row_array();

		echo json_encode($hasil);
	}
}	
	


public function simpan_stok_masuk()
{
	$data = $this->input->post();
	$jumlah = count($data['id_produk']);
	$waktu = date('Y-m-d h:i:s');

	if($data['status_tr'] == 'OP'){
		$status_nota = 'OP';
	}else if($data['status_tr'] == 'SM'){
		$status_nota = 'SM';
	}else if($data['status_tr'] == 'SK'){
		$status_nota = 'SK';
	}else{
		$status_nota = 'TR';
	}

	$no_nota = $status_nota.date('Ymdhis');
	
	for ($i=0; $i < $jumlah; $i++) { 

		if(!empty($data['id_produk'][$i]) && $data['id_produk'][$i] != ''){

			//$add['id_cabang'] = $data['cabang'];
			$add['id_produk'] = $data['id_produk'][$i];	
			$add['harga'] = $data['harga'][$i];
			$add['waktu'] = $waktu;
			$add['uid'] = $_SESSION['userid'];
			$add['status'] = $data['status'][$i];
			$add['no_nota'] = $no_nota;

			if($data['status'][$i] == 'in'){
				$add['masuk'] = $data['jumlah'][$i];
			}else if($data['status'][$i] == 'out'){
				$add['keluar'] = $data['jumlah'][$i];
			}else{
				$add['masuk'] = $data['jumlah'][$i];
			}
			$this->db->insert('produk_transaksi',$add);
		}
	}


	echo 1;
	
}

public function simpan_stok_op(){
	$data = $this->input->post();
	$jumlah = count($data['id_produk']);
	$waktu = date('Y-m-d h:i:s');
	$no_nota = 'OP'.date('Ymdhis');
	for ($i=0; $i < $jumlah; $i++) { 

		if(!empty($data['id_produk'][$i]) && $data['id_produk'][$i] != ''){
			
			$id_produk = $data['id_produk'][$i];

			$get = $this->db->query('SELECT (SUM(masuk) - SUM(keluar)) as sisa_stok , harga FROM produk_transaksi WHERE id_produk="'.$id_produk.'" ')->row_array();

			$jumlah = $data['jumlah'][$i];
			$sisa_stok = $get['sisa_stok'];

			if($sisa_stok > $jumlah){
				$add['masuk'] = $sisa_stok - $jumlah;
				$add['status'] = 'op';
			}else if($jumlah > $sisa_stok){
				$add['masuk'] = $jumlah - $sisa_stok;
				$add['status'] = 'op';
			}

			$add['id_produk'] = $data['id_produk'][$i];	
			$add['harga'] = $get['harga'];
			$add['waktu'] = $waktu;
			$add['uid'] = $_SESSION['userid'];
			$add['no_nota'] = $no_nota;

			
			$this->db->insert('produk_transaksi',$add);
		}
	}


	echo 1;
}

public function form_mercant($id='')
{
	$data['mercant'] = array();
	if($id != '')
	{
		$data['mercant'] = $this->db->get_where('data_mercant',array('id_mercant' => $id))->row_array();
	}

	$this->load->view('admin/header',null);
	$this->load->view('inventori/form_mercant',$data);
	$this->load->view('admin/footer');
}


public function simpan()
{
	$data = $this->input->post();

	$namefile ='';
	if(!empty($_FILES['foto']['name']))
	{
		$namefile = str_replace(' ', '-',$_FILES['foto']['name']);
		$config['upload_path']          = './assets/mercant/';
		$config['allowed_types']        = 'jpg|jpeg|png';
		$config['max_size']             = 2000;
		$confiq['file_name']						= $namefile;
							// $config['max_width']            = 1024;
							// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('foto'))
		{
			echo $this->upload->display_errors();
		}
		else
		{
												//echo 'success';
		}
	}

	$add['nama'] = $data['nama'];
	$add['alamat']= $data['alamat'];
	$add['disc'] = $data['dis'];
	$add['foto'] =$namefile;
	$add['ket'] = $data['ket'];
	$add['status'] = $data['status'];
	$add['input_date'] = date('Y-m-d');
	$add['latitude'] = $data['lat'];
	$add['longtitude'] = $data['long'];
	$add['sales_id'] = $_SESSION['userid'];

	if($this->db->insert('data_mercant',$add))
	{
		$id =  $this->db->insert_id();
		$user_id = $data['user_id'];
		$up['id_mercant'] = $id;
		$this->db->update('data_pelanggan',$up, array('id' => $user_id));

		if(isset($data['promo']))
		{
			$text = 'Dapatkan diskon '.$data['dis'].'% dengan kartu mamber di '.$data['nama'];
			push('New Merchant',$text);
		}
		echo 1;
	}




}

}
