<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaturan extends CI_Controller {


	public function __construct()
	{
		parent::__construct();

		if(!isset($_SESSION['userid']))
		{
			redirect(base_url().'masuk','refresh');
		}
	}

	function pagination($search,$item_per_page, $current_page, $total_records, $total_pages, $link=''){
		$pagination = '';

		$mindate = '';
		$maxdate= '';
		$status = '';


		if($mindate ==''){
			$mindate='all';
		}
		if($maxdate ==''){
			$maxdate='all';
		}
		if($status ==''){
			$status='all';
		}




		if($total_pages >= 0 && $current_page <= $total_pages){

			$pagination .= '<ul class="pagination">';


			$right_links    = $current_page + 10;
			$previous       = $current_page - 1;
			$next           = $current_page + 1;
			$first_link     = true;

			if($current_page > 1){
				$previous_link = ($previous==0)?1:$previous;
      $pagination .= '<li class="first page-item"><a class="page-link" href="'.base_url().$link.'1" title="First">&laquo;</a></li>'; //
      $pagination .= '<li class=" page-item"><a class="page-link" href="'.base_url().$link.'/'.$previous_link.'" title="Previous">&lt;</a></li>';
      for($i = ($current_page-2); $i < $current_page; $i++){
      	if($i > 0){
      		$pagination .= '<li class="page-item"><a class="page-link" href="'.base_url().$link.'/'.$i.'" title="Page'.$i.'">'.$i.'</a></li>';
      	}
      }
      $first_link = false;
  }

  if($first_link){
  	$pagination .= '<li class="first active page-item"><a class="page-link">'.$current_page.'</a></li>';
  }elseif($current_page == $total_pages){
  	$pagination .= '<li class="last active page-item"><a class="page-link">'.$current_page.'</a></li>';
  }else{
  	$pagination .= '<li class="active page-item"><a class="page-link">'.$current_page.'</a></li>';
  }

  for($i = $current_page+1; $i < $right_links ; $i++){
  	if($i<=$total_pages){
  		$pagination .= '<li class="page-item"><a class="page-link" href="'.base_url().$link.'/'.$i.'" title="Page '.$i.'">'.$i.'</a></li>';
  	}
  }

  if($current_page < $total_pages){

	      $pagination .= '<li class="page-item"><a class="page-link" href="'.base_url().$link.'/'.$next.'" title="Next">&gt;</a></li>'; //
	      $pagination .= '<li class="last page-item"><a class="page-link" href="'.base_url().$link.'/'.$total_pages.'" title="Last">&raquo;</a></li>';
	  }

	  $pagination .= '</ul>';
	}
	return $pagination;
}

public function index()
{
	$userid = $_SESSION['userid'];
	$data['mercant'] = $this->db->query("SELECT a.*,b.nama as pemilik FROM `data_mercant` a LEFt JOIN data_pelanggan b ON b.id_mercant = a.id_mercant WHERE a.status=1 AND a.sales_id='".$userid."'")->result_array();
	$this->load->view('admin/header',null);
	$this->load->view('pengaturan/mercant',$data);
	$this->load->view('admin/footer');
}

public function cabang()
{
	$userid = $_SESSION['userid'];
	$data['cabang'] = $this->db->query("SELECT a.*, b.nama as kota from cabang a LEFT JOIN t_kabupaten b ON a.id_kabupaten = b.id WHere a.id_user='".$userid."'")->result_array();

	$this->load->view('admin/header',null);
	$this->load->view('pengaturan/cabang',$data);
	$this->load->view('admin/footer');
}

public function akun(){
	$this->load->view('admin/header',null);
	$this->load->view('pengaturan/akun',null);
	$this->load->view('admin/footer');
}

public function pegawai()
{
	$userid = $_SESSION['userid'];
	$data['pegawai'] = $this->db->query("SELECT a.* , b.nama as level from pegawai a LEFT JOIN hak_akses b ON a.level = b.id")->result_array();
	$this->load->view('admin/header',null);
	$this->load->view('pengaturan/pegawai',$data);
	$this->load->view('admin/footer');
}

public function users()
{

	$data['users'] = $this->db->query("SELECT a.* , b.nama as level from admin a LEFT JOIN hak_akses b ON a.level = b.id")->result_array();
	$this->load->view('admin/header',null);
	$this->load->view('pengaturan/users',$data);
	$this->load->view('admin/footer');
}

public function pelanggan()
{
	if($_SESSION['level'] == 1){
		
		$data['pelanggan'] = $this->db->query("SELECT a.*,b.nama as cabang from pelanggan a LEFT JOIN cabang b ON a.id_cabang = b.id  ")->result_array();
	}else{
		$userid = $_SESSION['userid'];
		$data['pelanggan'] = $this->db->query("SELECT a.*,b.nama as cabang from pelanggan a LEFT JOIN cabang b ON a.id_cabang = b.id WHERE a.id_user='".$userid."' ")->result_array();
	}
	
	$this->load->view('admin/header',null);
	$this->load->view('pengaturan/pelanggan',$data);
	$this->load->view('admin/footer');
}


public function form_cabang($id='')
{
	$userid = $_SESSION['userid'];
	
	if($id != '')
	{
		$data['id'] = $id;
		$data['cabang'] = $this->db->get_where('cabang', array('id' => $id))->row_array();
	}
	$data['pajak'] = $this->db->query("SELECT * FROM pajak WHERE id_user= '".$userid."' ")->result_array();
	$this->load->view('admin/header',null);
	$this->load->view('pengaturan/form_cabang',$data);
	$this->load->view('admin/footer');
}

public function form_pegawai($id='')
{
	$data['hak_akses'] = $this->db->get('hak_akses')->result_array();
	if($id != '')
	{
		$data['id'] = $id;
		$data['pegawai'] = $this->db->get_where('pegawai', array('id_pegawai' => $id))->row_array();
	}
	$this->load->view('admin/header',null);
	$this->load->view('pengaturan/form_pegawai',$data);
	$this->load->view('admin/footer');
}

public function form_user($id='')
{
	$data['hak_akses'] = $this->db->get('hak_akses')->result_array();
	if($id != '')
	{
		$data['id'] = $id;
		$data['user'] = $this->db->get_where('admin', array('id' => $id))->row_array();
	}
	$this->load->view('admin/header',null);
	$this->load->view('pengaturan/form_user',$data);
	$this->load->view('admin/footer');
}

public function form_pelanggan($id='')
{
	
	if($id != '')
	{
		$data['id'] = $id;
		$data['pelanggan'] = $this->db->get_where('pelanggan', array('id' => $id))->row_array();
	}else{
		$data = '';
	}
	$this->load->view('admin/header',null);
	$this->load->view('pengaturan/form_pelanggan',$data);
	$this->load->view('admin/footer');
}

public function load_kota()
{
	$id = $this->input->post('id');
	$get = $this->db->get_where('t_kabupaten',  array('id_prov' => $id))->result_array();

	foreach ($get as $row) {
		echo "<option value='".$row['id']."'>".$row['nama']."</option>";
	}
}

public function load_kecamatan()
{
	$id = $this->input->post('id');
	$get = $this->db->get_where('t_kecamatan',  array('id_kabupaten' => $id))->result_array();

	foreach ($get as $row) {
		echo "<option value='".$row['id']."'>".$row['nama']."</option>";
	}
}

public function load_kelurahan()
{
	$id = $this->input->post('id');
	$get = $this->db->get_where('t_kelurahan',  array('id_kecamatan' => $id))->result_array();

	foreach ($get as $row) {
		echo "<option value='".$row['id']."'>".$row['nama']."</option>";
	}
}

public function simpan_cabang()
{
	$data = $this->input->post();

	$add['nama'] = $data['nama'];
	$add['alamat'] = $data['alamat'];
	$add['tlp'] = $data['tlp'];
	$add['id_provinsi'] = $data['provinsi'];
	$add['id_kabupaten'] = $data['kota'];
	$add['id_kecamatan'] = $data['kecamatan'];
	$add['id_kelurahan'] = $data['kelurahan'];
	$add['kode_nota'] = $data['kode_nota'];
	$add['id_user'] = $_SESSION['userid'];
	$add['status'] = $data['status'];

	if($data['pajak'] == ''){

	}else{
		$pajak = implode(',', $data['pajak']);

		$add['id_pajak'] = $pajak;
	}


	if(isset($data['id'])){
		if($this->db->update('cabang',$add, array('id' => $data['id'])))
		{
			echo 1;
		}
	}else{
		if($this->db->insert('cabang',$add))
		{
			echo 1;
		}
	}
}

public function simpan_pegawai()
{
	$data = $this->input->post();

	$add['nama'] = $data['nama'];
	// $add['id_cabang'] = $data['cabang'];
	$add['tlp'] = $data['tlp'];
	$add['id_user'] = $_SESSION['userid'];
	$add['status'] = $data['status'];
	$add['level'] = $data['level'];
	if(isset($data['id'])){
		if($this->db->update('pegawai',$add, array('id_pegawai' => $data['id'])))
		{
			echo 1;
		}
	}else{
		if($this->db->insert('pegawai',$add))
		{
			echo 1;
		}
	}
}

public function simpan_user()
{
	$data = $this->input->post();

	$add['nama'] = $data['nama'];
	$add['tlp'] = $data['tlp'];
	$add['status'] = $data['status'];
	$add['level'] = $data['level'];
	$add['email'] = $data['email'];
	if(isset($data['id'])){
		if($this->db->update('admin',$add, array('id' => $data['id'])))
		{
			echo 1;
		}
	}else{
		$add['pass'] = md5($data['password']);
		if($this->db->insert('admin',$add))
		{
			echo 1;
		}
	}
}

public function simpan_pelanggan()
{
	$data = $this->input->post();

	$get = $this->db->get_where('pelanggan', array('tlp' => $data['tlp']));
	$get_mail = $this->db->get_where('pelanggan', array('email' => $data['email']));

	if($get->num_rows() > 0){
		echo "Phone Number already exists";
		exit;
	}

	if($get_mail->num_rows() > 0){
		echo "Email already exists";
		exit;
	}

	$add['nama'] = $data['nama'];
	$add['kode_pos'] = $data['kode_pos'];
	$add['tlp'] = $data['tlp'];
	$add['id_user'] = $_SESSION['userid'];
	$add['alamat'] = $data['alamat'];
	$add['status'] = $data['status'];
	$add['email'] = $data['email'];
	$add['kota'] = $data['kota'];
	if(isset($data['id'])){
		if($this->db->update('pelanggan',$add, array('id' => $data['id'])))
		{
			echo 1;
		}
	}else{
		if($this->db->insert('pelanggan',$add))
		{
			echo 1;
		}
	}
}



}
