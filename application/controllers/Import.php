<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Import extends CI_Controller {


	public function __construct()
	{
		parent::__construct();

		if(!isset($_SESSION['userid']))
		{
			redirect(base_url().'masuk','refresh');
		}
	}

	public function upload()
	{
        // Load plugin PHPExcel nya
		include APPPATH.'third_party\PHPExcel\Classes\PHPExcel.php';

		$config['upload_path'] = realpath('excel');
		$config['allowed_types'] = 'xlsx|xls|csv';
		$config['max_size'] = '10000';
		$config['encrypt_name'] = true;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload()) {

            //upload gagal
			$this->session->set_flashdata('notif', '<div class="alert alert-danger"><b>PROSES IMPORT GAGAL!</b> '.$this->upload->display_errors().'</div>');
			echo "PROSES IMPORT GAGAL!";


		} else {

			$data_upload = $this->upload->data();

			$excelreader     = new PHPExcel_Reader_Excel2007();
            $loadexcel         = $excelreader->load('excel/'.$data_upload['file_name']); // Load file yang telah diupload ke folder excel
            $sheet             = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

            $data = array();

            $numrow = 1;
            foreach($sheet as $row){
            	if($numrow > 1){

            		$harga_24 = preg_replace('/\D/', '', $row['K']);
            		array_push($data, array(
            			'id_user' => $row['A'],
            			'id_supplier'      => $row['B'],
            			'id_kategori'      => 2,
            			'kode'      	   => $row['D'],
            			'nama'      	   => $row['E'],
            			'harga_beli'       => 0,
            			'harga'            => $row['G'],
            			'harga_3_satuan'   => $row['H'],
            			'harga_6_satuan'   => $row['I'],
            			'harga_12_satuan'  => $row['J'],
            			'harga_24_satuan'  => $harga_24,
            			'stok_alert'       => 100,
            			'ket'      		   => $row['M'],
            			'merek'      	   => $row['N'],
            			'negara'      	   => $row['O'],
            			'status'		   => 1,
            		));
            	}
            	$numrow++;
            }
            $this->db->insert_batch('produk', $data);
            //delete file from server
            unlink(realpath('excel/'.$data_upload['file_name']));

            //upload success
            $this->session->set_flashdata('notif', '<div class="alert alert-success"><b>PROSES IMPORT BERHASIL!</b> Data berhasil diimport!</div>');
            echo "PROSES IMPORT BERHASIL!</b> Data berhasil diimport!";
            

        }
    }

    // public function update_sql(){
    // 	$get_where = $this->db->query("SELECT * FROM produk ")->result_array();

    // 	foreach ($get_where as $row) {
    // 		$harga = preg_replace('/\D/', '', $row['harga']);
    // 		$data['harga'] = $harga;
    // 		//echo $harga;
    // 		if($this->db->update('produk', $data, array('id' => $row['id']))){
    // 			echo "berhasil".$row['nama']."<br>";
    // 		}
    // 	}
    // }

    // public function while(){
    // 	$x = 1;

    // 	while($x <= 1000000) {
    // 		echo "The number is: $x <br>";
    // 		$x++;
    // 	}
    // }
}

?>