<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Marketing extends CI_Controller {


  public function __construct()
  {
    parent::__construct();

    if(!isset($_SESSION['userid']))
    {
      redirect(base_url().'masuk','refresh');
    }else{
      $this->load->model('Adminmdl');
    }
  }

   public function barang()
  {
    
    $this->load->view('admin/header',null);
    $this->load->view('marketing/barang',null);
    $this->load->view('admin/footer');

  }

  public function barang_view(){
    $search = $_POST['search']['value'];
    $limit = $_POST['length']; 
    $start = $_POST['start']; 
    $order_index = $_POST['order'][0]['column']; 
    $order_field = $_POST['columns'][$order_index]['data']; 
    $order_ascdesc = $_POST['order'][0]['dir']; 

    $sql_total = $this->Adminmdl->count_all('produk'); 

    $load_data = $this->Adminmdl->filter($search, $limit, $start, $order_field, $order_ascdesc);
  

    $data_json = array();
    foreach ($load_data->result_array() as $row) {

        $data_json[] = array('id' => $row['id'], 
          'nama' => $row['nama'], 
          'nama_kategori' => $row['nama_kategori'], 
          'merek' => $row['merek'],
          'harga_beli' => decimals($row['harga_beli']),
          'harga' => decimals($row['harga_marketing']),
          'status' => $row['status']);
    }

   
    $sql_filter = $this->Adminmdl->count_filter($search);

    
    $callback = array(
        'draw'=>$_POST['draw'], 
        'recordsTotal'=>$sql_total,
        'recordsFiltered'=>$sql_filter,
        'data'=>$data_json
    );

    header('Content-Type: application/json');
    echo json_encode($callback); 
  }

}

?>