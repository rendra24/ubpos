<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {


	public function __construct()
	{
		parent::__construct();

		if(!isset($_SESSION['userid']))
		{
			redirect(base_url().'masuk','refresh');
		}
	}

	public function transaksi_penjualan($id_penjualan='')
	{	

	
		$where = '';
		$status = '';

		if($_SESSION['level'] == 1){
			if($post = $this->input->post()){
				if($post['tgl_awal'] != '' && $post['tgl_akhir'] != ''){

					if($post['status'] != ''){
						$status = " AND a.status_penjualan=".$post['status']." ";
						$data['status'] = $post['status'];
					}

					if($post['id_user'] != ''){
						$where = " WHERE DATE(a.tgl) >= '".$post['tgl_awal']."' AND DATE(a.tgl) <= '".$post['tgl_akhir']."' AND a.uid='".$post['id_user']."' ".$status;

						$data['tgl_awal'] = $post['tgl_awal'];
						$data['tgl_akhir'] = $post['tgl_akhir'];
						$data['id_user'] = $post['id_user'];
					}else{
						$where = " WHERE DATE(a.tgl) >= '".$post['tgl_awal']."' AND DATE(a.tgl) <= '".$post['tgl_akhir']."' ".$status;

						$data['tgl_awal'] = $post['tgl_awal'];
						$data['tgl_akhir'] = $post['tgl_akhir'];
					}

				}else{
					if($post['status'] != ''){
						$status = " AND a.status_penjualan=".$post['status']." ";
						$data['status'] = $post['status'];
					}
					$where = " WHERE a.uid='".$post['id_user']."' ".$status;
					$data['id_user'] = $post['id_user'];
				}
			}
		}


		if($_SESSION['level'] == 3){
			$where .= " WHERE a.uid ='".$_SESSION['userid']."' ";
			if($post = $this->input->post()){
			$where .= " AND DATE(a.tgl) >= '".$post['tgl_awal']."' AND DATE(a.tgl) <= '".$post['tgl_akhir']."' ";

						$data['tgl_awal'] = $post['tgl_awal'];
						$data['tgl_akhir'] = $post['tgl_akhir'];
			}
		}
		

		$get_penjualan= $this->db->query("SELECT e.nama as nama_customer, a.status_penjualan, a.id,a.tgl,GROUP_CONCAT(DISTINCT CONCAT(d.nama,' x ',b.qty,' <br> ') SEPARATOR '')as produk ,a.sub_total FROM penjualan a 
			LEFT JOIN penjualan_detail b ON a.id = b.id_penjualan 
			LEFT JOIN produk d ON b.id_produk = d.id 
			LEFT JOIN pelanggan e ON a.id_pelanggan = e.id 
			".$where."
			GROUP BY a.id ORDER BY a.id DESC");



		$user = $this->db->query("SELECT * FROM admin")->result_array();
		$pelanggan = $this->db->query("SELECT * FROM pelanggan")->result_array();

		$data['penjualan'] = $get_penjualan->result_array();
		$data['id_penjualan'] = $id_penjualan;
		$data['user'] = $user;
		$data['pelanggan'] = $pelanggan;

		$this->load->view('admin/header',null);
		$this->load->view('laporan/penjualan',$data);
		$this->load->view('admin/footer');

	}

	public function update_pelanggan(){
		if($post = $this->input->post()){
			$data['id_pelanggan'] = $post['id_pelanggan'];

			if($this->db->update('penjualan',$data, array('id' => $post['id_penjualan']))){
				//echo $this->db->last_query();
				echo 1;
			}
		}
	}

	public function get_detail_struk(){
		if($post = $this->input->post()){
			$data['id_penjualan'] = $post['id_penjualan'];
			$this->load->view('laporan/load_detail_struk',$data);
		}
	}

	public function invoice($id=''){
		$data['detail'] = $this->db->query("SELECT a.*, b.nama , b.merek FROM penjualan_detail a 
			LEFT JOIN produk b ON a.id_produk = b.id 
			 WHERE a.id_penjualan=$id")->result_array();

		$data['pen'] = $this->db->query("SELECT a.*, b.nama , b.kota , b.kode_pos, b.alamat, b.tlp FROM penjualan a 
		LEFT JOIN pelanggan b ON a.id_pelanggan = b.id WHERE a.id=$id ")->row_array();

		$this->load->view('laporan/invoice',$data);
	}

	public function stok(){

		$sql = "SELECT a.id_produk, b.nama , c.nama_kategori, b.kode , SUM(a.masuk) as masuk ,  SUM(a.keluar) as keluar , a.harga FROM produk_transaksi a 
		LEFT JOIN produk b ON a.id_produk = b.id
		LEFT JOIN kategori c ON b.id_kategori = c.id_kategori
		GROUP BY id_produk";

		$get_stok = $this->db->query($sql)->result_array();

		$data['stok'] = $get_stok;

		$this->load->view('admin/header',null);
		$this->load->view('laporan/stok',$data);
		$this->load->view('admin/footer');

	}

	public function kas(){

		$where = '';
		if($post = $this->input->post()){
			$where = " WHERE DATE(a.tgl) >= '".$post['tgl_awal']."' AND DATE(a.tgl) <= '".$post['tgl_akhir']."' ";
		}
		$sql = "SELECT a.id, a.tgl , a.`status`, a.keterangan , a.total FROM kas a  ".$where;

		$kas = $this->db->query($sql)->result_array();

		$data['kas'] = $kas;

		$this->load->view('admin/header',null);
		$this->load->view('laporan/kas',$data);
		$this->load->view('admin/footer');

	}

	public function laba_produk(){
		$sql = "SELECT a.id, a.nama , b.harga as harga_jual , SUM(c.sub_total) as total_penjualan , (SUM(d.masuk) * d.harga) as total_pembelian , SUM(b.profit) as laba FROM produk a 
		LEFT JOIN penjualan_detail b ON a.id = b.id_produk
		LEFT JOIN penjualan c ON c.id = b.id_penjualan
		LEFT JOIN produk_transaksi d ON a.id = d.id_produk
		WHERE d.`status` = 'in'
		GROUP BY b.id_produk";

		$laba = $this->db->query($sql)->result_array();

		$data['laba'] = $laba;

		$this->load->view('admin/header',null);
		$this->load->view('laporan/laba_produk',$data);
		$this->load->view('admin/footer');
	}

}

?>