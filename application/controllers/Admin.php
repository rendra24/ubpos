<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {


  public function __construct()
  {
    parent::__construct();

    if(!isset($_SESSION['userid']))
    {
      redirect(base_url().'masuk','refresh');
    }else{
      $this->load->model('Adminmdl');
    }
  }


  public function index()
  {

    $data = array(
     'aktif' => 'blog',
     'judul' => 'BLOG',
   );

    $this->load->view('admin/header',$data);
    $this->load->view('admin/home');
    $this->load->view('admin/footer');

  }


  function pagination($search,$item_per_page, $current_page, $total_records, $total_pages, $link=''){
    $pagination = '';

    $mindate = '';
    $maxdate= '';
    $status = '';


    if($mindate ==''){
      $mindate='all';
    }
    if($maxdate ==''){
      $maxdate='all';
    }
    if($status ==''){
      $status='all';
    }




    if($total_pages >= 0 && $current_page <= $total_pages){

      $pagination .= '<ul class="pagination">';


      $right_links    = $current_page + 10;
      $previous       = $current_page - 1;
      $next           = $current_page + 1;
      $first_link     = true;

      if($current_page > 1){
        $previous_link = ($previous==0)?1:$previous;
      $pagination .= '<li class="first page-item"><a class="page-link" href="'.base_url().$link.'1" title="First">&laquo;</a></li>'; //
      $pagination .= '<li class=" page-item"><a class="page-link" href="'.base_url().$link.'/'.$previous_link.'" title="Previous">&lt;</a></li>';
      for($i = ($current_page-2); $i < $current_page; $i++){
        if($i > 0){
          $pagination .= '<li class="page-item"><a class="page-link" href="'.base_url().$link.'/'.$i.'" title="Page'.$i.'">'.$i.'</a></li>';
        }
      }
      $first_link = false;
  }

  if($first_link){
    $pagination .= '<li class="first active page-item"><a class="page-link">'.$current_page.'</a></li>';
  }elseif($current_page == $total_pages){
    $pagination .= '<li class="last active page-item"><a class="page-link">'.$current_page.'</a></li>';
  }else{
    $pagination .= '<li class="active page-item"><a class="page-link">'.$current_page.'</a></li>';
  }

  for($i = $current_page+1; $i < $right_links ; $i++){
    if($i<=$total_pages){
      $pagination .= '<li class="page-item"><a class="page-link" href="'.base_url().$link.'/'.$i.'" title="Page '.$i.'">'.$i.'</a></li>';
    }
  }

  if($current_page < $total_pages){

        $pagination .= '<li class="page-item"><a class="page-link" href="'.base_url().$link.'/'.$next.'" title="Next">&gt;</a></li>'; //
        $pagination .= '<li class="last page-item"><a class="page-link" href="'.base_url().$link.'/'.$total_pages.'" title="Last">&raquo;</a></li>';
    }

    $pagination .= '</ul>';
  }
  return $pagination;
}

public function list_tgl(){
  
$this->load->view('print');

}

  public function detail_barang()
  {
    $id = $this->input->post('id');
    //$id= 1;
    $get = $this->db->get_where('data_barang', array('id_barang' => $id));
    echo json_encode($get->row_array());
  }

  public function temp_penjualan_app()
  {
    $data = $this->input->post();

    $get = $this->db->get_where('temp_penjualan_app', array('id_barang' => $data['id_barang'], 'uid' => $data['uid']));
    if($get->num_rows() > 0)
    {

    }else{
      $add['id_barang'] = $data['id_barang'];
      $add['qty'] = $data['qty'];
      $add['uid'] = $data['uid'];
      $add['tanggal'] = date('Y-m-d');
      $add['user_id'] = $data['uid'];

      if($this->db->insert('temp_penjualan_app',$add))
      {
        echo 1;
      }
    }

    

  }

  public function cek_login_apps()
  {
    $in = $this->input->post();

    if($in){
      $username = $in['username'];
      $pass = $in['password'];
    // $username = 'M123';
    // $pass='ana';
      $get = $this->db->query("SELECT * from data_pelanggan WHERE kode='".$username."' AND pass='".$pass."'");

      if($get->num_rows() > 0)
      {
        $row = $get->row_array();
        $data['nama'] = $row['nama'];
        $data['kode'] = $row['kode'];
        $data['user_id'] = $row['id'];

        echo json_encode($data);
      }
    }
    
  }

  public function get_detail_produk(){
    if($post = $this->input->post()){

        $get = $this->db->query('SELECT a.*, b.nama_kategori , c.nama_satuan , d.nama as nama_supplier FROM produk a 
        LEFT JOIN kategori b ON a.id_kategori = b.id_kategori
        LEFT JOIN t_unit c ON a.satuan = c.id 
        LEFT JOIN supplier d ON a.id_supplier = d.id
        WHERE a.id="'.$post['id_produk'].'" ')->row_array();

        $json['nama'] = $get['nama'];
        $json['barcode'] = $get['kode'];
        $json['kategori'] = $get['nama_kategori'];
        $json['unit'] = $get['nama_satuan'];
        $json['supplier'] = $get['nama_supplier'];
        $json['stok'] = $get['stok_alert'];
        $json['keterangan'] = $get['ket'];

        $json['harga_beli'] = 'Rp'.decimals($get['harga_beli']);
        $json['harga_marketing'] = 'Rp'.decimals($get['harga_marketing']);
        $json['harga'] = 'Rp'.decimals($get['harga']);
        $json['harga_3_satuan'] = 'Rp'.decimals($get['harga_3_satuan']);
        $json['harga_6_satuan'] = 'Rp'.decimals($get['harga_6_satuan']);
        $json['harga_12_satuan'] = 'Rp'.decimals($get['harga_12_satuan']);
        $json['harga_24_satuan'] = 'Rp'.decimals($get['harga_24_satuan']);

        echo json_encode($json);
    }
  }

  public function barang()
  {
 
    // $get_barang = $this->db->query("SELECT a.*,b.nama_kategori FROM produk a LEFT JOIN kategori b ON a.id_kategori = b.id_kategori WHERE a.id_user=".$_SESSION['userid']);
    // $data['barang'] = $get_barang->result_array();

    $this->load->view('admin/header',null);
    $this->load->view('admin/barang',null);
    $this->load->view('admin/footer');

  }

  public function barang_view(){
    $search = $_POST['search']['value']; // Ambil data yang di ketik user pada textbox pencarian
    $limit = $_POST['length']; // Ambil data limit per page
    $start = $_POST['start']; // Ambil data start
    $order_index = $_POST['order'][0]['column']; // Untuk mengambil index yg menjadi acuan untuk sorting
    $order_field = $_POST['columns'][$order_index]['data']; // Untuk mengambil nama field yg menjadi acuan untuk sorting
    $order_ascdesc = $_POST['order'][0]['dir']; // Untuk menentukan order by "ASC" atau "DESC"

    $sql_total = $this->Adminmdl->count_all('produk'); // Panggil fungsi count_all pada SiswaModel

    $load_data = $this->Adminmdl->filter($search, $limit, $start, $order_field, $order_ascdesc);
  

    $data_json = array();
    foreach ($load_data->result_array() as $row) {

        $data_json[] = array('id' => $row['id'], 
          'nama' => $row['nama'], 
          'nama_kategori' => $row['nama_kategori'], 
          'merek' => $row['merek'],
          'harga_beli' => decimals($row['harga_beli']),
          'harga_marketing' => decimals($row['harga_marketing']),
          'harga' => decimals($row['harga']),
          'status' => $row['status']);
    }

   
    $sql_filter = $this->Adminmdl->count_filter($search);

    
    $callback = array(
        'draw'=>$_POST['draw'], // Ini dari datatablenya
        'recordsTotal'=>$sql_total,
        'recordsFiltered'=>$sql_filter,
        'data'=>$data_json
    );

    header('Content-Type: application/json');
    echo json_encode($callback); // Convert array $callback ke json
  }

  public function kategori()
  {
    $get_kategori = $this->db->query("SELECT * FROM kategori ");
    $data['kategori'] = $get_kategori->result_array();

    $this->load->view('admin/header',null);
    $this->load->view('admin/kategori',$data);
    $this->load->view('admin/footer');

  }

  public function kas()
  {
    $get_kas = $this->db->query("SELECT * FROM data_arus_kas a LEFT JOIN kode_arus_kas b ON a.id_kas=b.id_kas");
    $data['kas'] = $get_kas->result_array();

    $this->load->view('admin/header',null);
    $this->load->view('admin/kas',$data);
    $this->load->view('admin/footer');

  }

  public function gudang()
  {
    $get_kas = $this->db->query("SELECT * FROM kode_gudang a LEFT JOIN kode_cabang b ON a.id_cabang=b.id_cabang ");
    $data['gudang'] = $get_kas->result_array();

    $this->load->view('admin/header',null);
    $this->load->view('admin/gudang',$data);
    $this->load->view('admin/footer');

  }

  public function cabang()
  {
    $get_kas = $this->db->query("SELECT * FROM kode_cabang ");
    $data['cabang'] = $get_kas->result_array();

    $this->load->view('admin/header',null);
    $this->load->view('admin/cabang',$data);
    $this->load->view('admin/footer');

  }

  public function form_barang($id='')
  {

    $data['unit'] = $this->db->get('t_unit')->result_array();
    if($id != '')
    {
      $get_barang = $this->db->query("SELECT * FROM produk WHERE id= '".$id."'");
      $data['barang'] = $get_barang->row_array();
      $data['id'] = $id;
    }


    $this->load->view('admin/header',null);
    $this->load->view('admin/form_barang',$data);
    $this->load->view('admin/footer');
  }

  public function form_gudang($id='')
  { 

    if($id != '')
    {
      $get_barang = $this->db->query("SELECT * FROM kode_gudang WHERE id_gudang= '".$id."'");
      $data['gudang'] = $get_barang->row_array();
      $data['id'] = $id;
    }

    $get_cabang = $this->db->query("SELECT * FROM kode_cabang ");
    $data['cabang'] = $get_cabang->result_array();

    $this->load->view('admin/header',null);
    $this->load->view('admin/form_gudang',$data);
    $this->load->view('admin/footer');
  }

  public function form_kategori($id='')
  {
    
    if($id != '')
    {
      $get_barang = $this->db->query("SELECT * FROM kategori WHERE id_kategori= '".$id."'");
      $data['kategori'] = $get_barang->row_array();
      $data['id'] = $id;
    }else{
      $data = '';
    }

    $this->load->view('admin/header',null);
    $this->load->view('admin/form_kategori',$data);
    $this->load->view('admin/footer');
  }

  public function form_cabang($id='')
  {

    if($id != '')
    {
      $get_barang = $this->db->query("SELECT * FROM kode_cabang WHERE id_cabang= '".$id."'");
      $data['cabang'] = $get_barang->row_array();
      $data['id'] = $id;
    }else{
      $data='';
    }

    $this->load->view('admin/header',null);
    $this->load->view('admin/form_cabang',$data);
    $this->load->view('admin/footer');
  }

  public function form_kas()
  {

    $get_kas = $this->db->query("SELECT * FROM kode_arus_kas ");
    $data['kas'] = $get_kas->result_array();

    $this->load->view('admin/header',null);
    $this->load->view('admin/form_kas',$data);
    $this->load->view('admin/footer');
  }


  public function simpan_barang()
  {
    $data = $this->input->post();
    

    if(!empty($_FILES['files']['name']) && count(array_filter($_FILES['files']['name'])) > 0){ 
      $filesCount = count($_FILES['files']['name']); 
      for($i = 0; $i < $filesCount; $i++){ 

          $_FILES['file']['name'] = $_FILES['files']['name'][$i];
          $_FILES['file']['type'] = $_FILES['files']['type'][$i];
          $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
          $_FILES['file']['error'] = $_FILES['files']['error'][$i];
          $_FILES['file']['size'] = $_FILES['files']['size'][$i];


        $config['upload_path']          = "./foto_produk/";
        $config['file_name']            = $_FILES['files']['name'][$i];
        $config['allowed_types']        = "gif|jpg|png|jpeg";
        $config['max_size']             = 30000;

        $files[] = $_FILES['files']['name'][$i];
        

        $this->load->library('upload', $config); 
        

        if(!$this->upload->do_upload('file')){ 
          echo $this->upload->display_errors();
        }else{  
          $uploadData = $this->upload->data();
          $img['nama_berkas'] = $uploadData['file_name'];
          $img['tipe_berkas'] = $uploadData['file_ext'];
          $img['ukuran_berkas'] = $uploadData['file_size'];

          
        } 
      }

      $attachment = implode(', ',$files);
       $input['foto'] = $attachment;
    }

    if(isset($data['status'])){
    $status = 1;
  }else{
    $status = 0;
  }


    $input['id_supplier'] = $data['supplier'];
    $input['id_user'] = $_SESSION['userid'];
    $input['id_kategori'] = $data['id_kategori'];
    $input['nama'] = $data['nama_produk'];
    $input['nama_eng'] = $data['nama_produk_eng'];
    $input['satuan'] =$data['satuan'];
    $input['harga'] = $data['harga1'];
    $input['harga_marketing'] = $data['harga_marketing'];
    $input['harga_3_satuan'] = $data['harga3'];
    $input['harga_6_satuan'] = $data['harga6'];
    $input['harga_12_satuan'] = $data['harga12'];
    $input['harga_24_satuan'] = $data['harga24'];
    $input['ket'] = $data['keterangan'];
    $input['kode'] = $data['barcode'];
    $input['status'] = 1;
    $input['stok_alert'] = $data['stok_alert'];
    $input['harga_beli'] = $data['harga_beli'];
    $input['status'] = $status;



    if(isset($data['id_barang']))
    {
      $id = $data['id_barang'];

      if($this->db->update('produk', $input, array('id' => $id)))
      {
        echo 1;
      }else{
        echo 0;
      }
    }else{

     if($this->db->insert('produk', $input))
     {
      echo 1;
    }else{
      echo 0;
    }

  }

  }

  public function simpan_kategori()
  {
    $data = $this->input->post();

    $input['nama_kategori'] = $data['nama_kategori'];
    $input['status'] = 1;

    if(isset($data['id_kategori']))
    {
      $id = $data['id_kategori'];
      if($this->db->update('kategori', $input, array('id_kategori' => $id)))
      {
        echo 1;
      }else{
        echo 0;
      }

    }else{
     if($this->db->insert('kategori',$input))
     {
      echo 1;
    }else{
      echo 0;
    }
  }


}

public function simpan_cabang()
{
  $data = $this->input->post();

  $input['nama_cabang'] = $data['nama_cabang'];

  if(isset($data['id_cabang']))
  {
    $id = $data['id_cabang'];
    if($this->db->update('kode_cabang', $input, array('id_cabang' => $id)))
    {
      echo 1;
    }else{
      echo 0;
    }

  }else{
   if($this->db->insert('kode_cabang',$input))
   {
    echo 1;
  }else{
    echo 0;
  }
}
}

public function simpan_gudang()
{
  $data = $this->input->post();

  $input['nama_gudang'] = $data['nama_gudang'];
  $input['id_cabang'] = $data['id_cabang'];

  if(isset($data['id_gudang']))
  {
    $id = $data['id_gudang'];
    if($this->db->update('kode_gudang', $input, array('id_gudang' => $id)))
    {
      echo 1;
    }else{
      echo 0;
    }

  }else{
   if($this->db->insert('kode_gudang',$input))
   {
    echo 1;
  }else{
    echo 0;
  }
}

}



public function simpan_kas()
{
  $data = $this->input->post();

  $input['id_kas'] = $data['kas'];
  $input['waktu'] = $data['waktu'];
  $input['masuk'] = $data['masuk'];
  $input['keluar'] = $data['keluar'];
  $input['keterangan'] = $data['keterangan'];
  $input['id_user'] = $_SESSION['userid'];

  if(isset($data['id']))
  {
    $id = $data['id'];
    if($this->db->update('data_arus_kas', $input, array('id' => $id)))
    {
      echo 1;
    }else{
      echo 0;
    }

  }else{
   if($this->db->insert('data_arus_kas',$input))
   {
    echo 1;
  }else{
    echo 0;
  }
}


}


public function remove_barang()
{
  $id = $this->input->post('id');
  $data['status'] = 0;
  if($this->db->update('produk', $data , array('id' => $id)))
  {
    echo 1;
  }else{
    echo 0;
  }
}

public function remove_kategori()
{
  $id = $this->input->post('id_kategori');
 $data['status'] = 0;
  if($this->db->update('kode_kategori',$data , array('id_kategori' => $id)))
  {
    echo 1;
  }else{
    echo 0;
  }
}

public function remove_cabang()
{
  $id = $this->input->post('id_cabang');

  if($this->db->delete('kode_cabang',array('id_cabang' => $id)))
  {
    echo 1;
  }else{
    echo 0;
  }
}


public function remove_gudang()
{
  $id = $this->input->post('id_gudang');

  if($this->db->delete('kode_gudang',array('id_gudang' => $id)))
  {
    echo 1;
  }else{
    echo 0;
  }
}



}
