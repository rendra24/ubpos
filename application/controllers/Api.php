<?php
header('Access-Control-Allow-Origin: *');  

class Api extends CI_Controller {
  
  public function __construct()
  {
    parent::__construct();


  }

  public function login_kasir()
  {
    $data = $this->input->post();
    $get = $this->db->query('SELECT a.*,b.nama_usaha FROM pegawai a LEFT JOIN admin b ON a.id_user = b.id WHERE a.email ="'.$data['email'].'" AND a.password="'.md5($data['password']).'"');

    if($get->num_rows() > 0)
    {
      $result = $get->row_array();
      echo json_encode($result);
    }
  }
  
  public function move_sql(){
      $data = $this->input->post();
      
      $get = $this->db->get_where('temp_penjualan',array('id_temp' => $data['id']));
      if($get->num_rows() == 0){
         
      $add['id_temp'] = $data['id'];
      $add['tanggal_order'] = $data['tanggal_order'];
      $add['total'] = $data['total'];
      $add['id_user'] = $data['id_user'];
      $add['id_cabang'] = $data['id_cabang'];
      $add['bayar'] = $data['bayar'];
      $add['kembali'] = $data['kembali'];
      $this->db->insert('temp_penjualan',$add);
      
      }
      
  }
  
  public function move_product()
  {
      $data = $this->input->post();
      
    //   $get = $this->db->get_where('temp_penjualan_detail',array('id_temp_penjualan' => $data['id']));
    //   if($get->num_rows() == 0){
         
      $add['id_temp_penjualan'] = $data['id'];
      $add['id_produk'] = $data['id_produk'];
      $add['total'] = $data['total'];
      $add['qty'] = $data['qty'];
      $add['harga'] = $data['harga'];
      $this->db->insert('temp_penjualan_detail',$add);
      
      //}
  }
  
  public function move_penjualan_apps(){
        $data = $this->input->post();
        $tanggal = date('Y-m-d h:i:s');
      $get = $this->db->get_where('temp_penjualan',array('id_temp' => $data['id']));
      $get_produk = $this->db->query("SELECT * from temp_penjualan_detail WHERE id_temp_penjualan = '".$data['id']."' ")->result_array();
      if($get->num_rows() > 0){
          $hasil = $get->result_array();
          
          foreach($hasil as $row){
                $get_nota = $this->db->query("SELECT kode_nota,id_user from cabang WHERE id = '".$row['id_cabang']."'")->row_array();
              
                $kode_nota = $get_nota['kode_nota'].date('Ymdhis');

                $up['id_cabang'] = $row['id_cabang'];
                $up['no_nota'] = $kode_nota;
                $up['uid'] = $row['id_user'];
                $up['sub_total'] = $row['total'];
                $up['bayar'] = $row['bayar'];
                $up['kembali'] = $row['kembali'];
                
               if($this->db->insert('penjualan',$up)){
                     $id_last = $this->db->insert_id();

                      foreach($get_produk as $rows)
                      {
                        $add['id_penjualan'] = $id_last;
                        $add['id_produk'] = $rows['id_produk'];
                        $add['qty'] = $rows['qty'];
                        $add['harga'] = $rows['harga'];
                        $add['total'] = $rows['total'];
                        $add['tgl'] = $tanggal;
                
                        $this->db->insert('penjualan_detail',$add);
                
                        $tr['id_cabang'] = $row['id_cabang'];
                        $tr['no_nota'] = $kode_nota;
                        $tr['id_produk'] = $rows['id_produk'];
                        $tr['keluar'] = $rows['qty'];
                        $tr['harga'] = $rows['harga'];
                        $tr['waktu'] = $tanggal;
                        $tr['uid'] = $get_nota['id_user'];
                        $tr['status'] = 'out';
                
                        $this->db->insert('produk_transaksi',$tr);
                      }
                      
                      $this->db->delete('temp_penjualan',array('id_temp' => $data['id']));
                      $this->db->delete('temp_penjualan_detail',array('id_temp_penjualan' => $data['id']));
                
                      $json['status'] = 1;
                      $json['id'] = $id_last;
                
                      echo json_encode($json);
                   
               }
          }
      }
  }
  
  public function move_temp()
  {
    $data = $this->input->post();

    $tanggal = date('Y-m-d h:i:s');
    $pen = $this->db->query("SELECT * from temp_penjualan WHERE id_temp = '".$data['id_temp']."' ")->row_array();

    $get = $this->db->query("SELECT * from temp_penjualan_detail WHERE id_temp_penjualan = '".$data['id_temp']."' ")->result_array();
    $get_nota = $this->db->query("SELECT kode_nota,id_user from cabang WHERE id = '".$data['id_cabang']."'")->row_array();

    $kode_nota = $get_nota['kode_nota'].date('Ymdhis');

    $up['id_cabang'] = $data['id_cabang'];
    $up['no_nota'] = $kode_nota;
    $up['uid'] = $get_nota['id_user'];
    $up['sub_total'] = $pen['total'];
    $up['bayar'] = $data['jumlah'];
    $up['kembali'] = $data['jumlah'] - $pen['total'];

    if($this->db->insert('penjualan',$up)){

      $id_last = $this->db->insert_id();

      foreach($get as $row)
      {
        $add['id_penjualan'] = $id_last;
        $add['id_produk'] = $row['id_produk'];
        $add['qty'] = $row['qty'];
        $add['harga'] = $row['harga'];
        $add['total'] = $row['total'];
        $add['tgl'] = $tanggal;

        $this->db->insert('penjualan_detail',$add);

        $tr['id_cabang'] = $data['id_cabang'];
        $tr['no_nota'] = $kode_nota;
        $tr['id_produk'] = $row['id_produk'];
        $tr['keluar'] = $row['qty'];
        $tr['harga'] = $row['harga'];
        $tr['waktu'] = $tanggal;
        $tr['uid'] = $get_nota['id_user'];
        $tr['status'] = 'out';

        $this->db->insert('produk_transaksi',$tr);
      }

      $json['status'] = 1;
      $json['id'] = $id_last;

      echo json_encode($json);
      
    }
    
    
  }
  public function get_kategori()
  {
  	$data = $this->db->get('kategori')->result_array();

  	echo json_encode($data);
  }

  public function get_temp()
  {
    $id = $this->input->post('id');
    $data = $this->db->query("SELECT b.*,c.nama from temp_penjualan a LEFT JOIN temp_penjualan_detail b ON a.id_temp = b.id_temp_penjualan LEFT JOIN produk c ON b.id_produk = c.id WHERE a.id_temp = '".$id."'")->result_array();

    foreach ($data as $row) {
      echo " <tr>
      <td>
      <p>".$row['qty']."</p>
      </td>
      <td>".$row['nama']."</td>
      <td>".format_decimals($row['total'])."</td>
      <input type='hidden' value='".$row['total']."' class='qtytxt'
      </tr>";
    }


  }

    public function printer(){
    $id = $this->input->post('id');
    $get = $this->db->query('SELECT a.*,b.nama  ,(21 - CHAR_LENGTH(b.nama)) - (CHAR_LENGTH(qty)) as hasil_jml, 6 - (CHAR_LENGTH(a.total)) as total_rp
      from penjualan_detail a 
      LEFT JOIN produk b ON a.id_produk = b.id WHERE a.id_penjualan="'.$id.'"');
    if($get->num_rows() > 0)
    {
      $hasil = $get->result_array();
      $cart = '';
      foreach ($hasil as $row) {
        $total = decimals($row['total']);
        $cart[] = array('nama' => $row['nama'], 'qty' => $row['qty'], 'total' => $total, 'hasil_jml' => $row['hasil_jml']);
    }

    echo json_encode($cart);



    }
  }
  
  public function head_print()
  {
    $id = $this->input->post('id');
    $get = $this->db->query("SELECT a.*,b.nama_usaha,6 - (CHAR_LENGTH(a.bayar)) as txt_bayar,6 - (CHAR_LENGTH(a.kembali)) as txt_kembali,6 - (CHAR_LENGTH(a.sub_total)) as txt_total from penjualan a LEFT JOIN admin b ON a.uid = b.id WHERE a.id= '".$id."' ");

    if($get->num_rows() > 0)
    {
      $hasil = $get->row_array();

      $json['no']=$hasil['id'];
      $json['tanggal'] = date('d-m Y', strtotime($hasil['tgl']));
      $json['nama_usaha'] = $hasil['nama_usaha'];
      $json['bayar'] = decimals($hasil['bayar']);
      $json['kembali'] = decimals($hasil['kembali']);
      $json['total'] = decimals($hasil['sub_total']);
      $json['txt_total'] = $hasil['txt_total'];
      $json['txt_kembali'] = $hasil['txt_kembali'];
      $json['txt_bayar'] = $hasil['txt_bayar'];

      echo json_encode($json);
    }
  }

  public function get_temp_total()
  {
    $id = $this->input->post('id');
    $data = $this->db->query("SELECT * from temp_penjualan WHERE id_temp='".$id."' ")->row_array();

    $json['total'] = format_decimals($data['total']);
    echo json_encode($json);
  }

  public function temp_penjualan()
  {
    $data = $this->input->post();

    $total = 0;

    $add['tanggal_order'] = date('Y-m-d h:i:s');
    $add['id_user'] = $data['id_user'];
    $this->db->insert('temp_penjualan',$add);
    $id_last = $this->db->insert_id();
    $get = $this->db->get_where('produk', array('id_user' => $data['id_user']))->result_array();
    $i = 0;
    foreach ($get as $row) {
      if($data['qty'.$i] > 0)
      {
        $total += $data['qty'.$i] * $row['harga'];
        $up['id_temp_penjualan'] = $id_last;
        $up['id_produk'] = $data['id_produk'.$i];
        $up['qty'] = $data['qty'.$i];
        $up['total'] = $data['qty'.$i] * $row['harga'];
        $up['harga'] = $row['harga'];

        $this->db->insert('temp_penjualan_detail',$up);
      }
      $i++;
    }

    $us['total'] = $total;
    $this->db->update('temp_penjualan',$us, array('id_temp' => $id_last));

    //  $this->db->delete('temp_penjualan', array('id_temp' => $id_last-1));
    //  $this->db->delete('temp_penjualan_detail', array('id_temp_penjualan' => $id_last-1));

    $json['id'] =  $id_last;
    echo json_encode($json);

  }

  public function get()
  {
    $get = $this->db->get_where('produk', array('id_user' => 1))->result_array();
    $i = 1;
    foreach ($get as $row) {

      echo ' <div id="divproduk'.$i.'" class="divproduk" key="'.$i.'" style="width:100%;padding: 15px 15px;border-bottom: 1px solid #DDD;">
      <div class="row">
      <div class="col-70 produk" key="'.$i.'">
      <div class="row">
      <div class="col-40">
      <img src="http://lorempixel.com/160/160/people/1" width="80" class="img-produk'.$i.'">
      <button type="button" class="col button button-fill jumlahp'.$i.' btn-jml-produk"></button>
      <input type="hidden" value="0" id="jml_text'.$i.'" name="qty'.$row['id'].'">
      <input type="hidden" value="'.$row['id'].'" name="id_produk'.$row['id'].'">

      </div>
      <div class="col-60">
      <div class="item-title-row">
      <div class="item-title">'.$row['nama'].'</div>
      </div>
      <div class="item-text">'.format_decimals($row['harga']).'</div>
      </div>
      </div>
      </div>

      <div class="col-30" style="text-align:right;">
      <button type="button" class="col button button-fill btnmin btn-min-produk" key="'.$i.'"  id="btnmin'.$i.'">-</button>
      </div>
      </div>
      </div>';

      $i++;
    }

  }

  public function get_kembali()
  {
    $id= $this->input->post('id');
    $get = $this->db->get_where('penjualan', array('id' => $id))->row_array();
    $json['kembali'] = format_decimals($get['kembali']);
    echo json_encode($json);
  }
  
  public function get_product_json()
  {
      $this->db->select('id,id_kategori,nama,harga,satuan');
    $get = $this->db->get_where('produk', array('id_user' => 1))->result_array();

    
    echo json_encode($get);

  }





}
