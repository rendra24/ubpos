
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>SARIRAYA Invoice PrintOut</title>
	<style type="text/css">
		body {
			font: 13px/1.6em arial, verdana, tahoma, sans-serif;
			background-color:#ffffff;
		}

		#wrap {
			position:relative;
			width: 750px;
			min-height:600px;
			margin: auto;
		}

		#customerForm{
			width:350px;
			height:120px;
			float:left;
			border: 4px solid #f79646;
			border-radius:35px;
			padding:20px;
		}

		#shopForm{
			width:320px;
			float:left;
			margin-left:30px;
		}

		#content{
			width:670px;
			float:left;

		}

		#footer{
			width:650px;
			float:left;
			border: 4px solid #f79646;
			padding:20px 5px 5px 5px;
			margin-top:10px;
		}

		.tb_result{
			border-collapse:collapse;
		}

		.tb_result td{
			color:#000000;
			background-color:#FFFFFF;
			border: 1px solid #000000;
		}

		.tb_result th{
			color:#000000;
			background-color:#FFFFFF;
			border: 1px solid #000000;
		}
	</style>
</head>
<body onload="window.print()"> 
	<!-- <body> -->
		<div id="wrap">
			<div id="customerForm">
				<table width="100%">
					<tr>
						<td>〒 - <?php echo $pen['kode_pos']; ?>, <br> <?php echo $pen['alamat']; ?> <?php echo $pen['kota']; ?></td>
					</tr>
					<tr>
						<td></td>
					</tr>
					<tr style="height:30px;font-size:18px;font-weight:bold;">
						<td><?php echo $pen['nama']; ?></td>
						<td></td>
					</tr>
					<tr style="height:30px;font-size:18px;">
						<td style="text-align: right;">ＴＥＬ　： <?php echo $pen['tlp']; ?></td>
					</tr>
				</table>
			</div>
			<div id="shopForm">
				<table width="70%">
					<tr style="height:30px;background-color:#ffc000;font-size:25px;font-weight:bold;">
						<td colspan="2" align="center">請求明細書/納品書</td>
					</tr>
					<tr style="margin-bottom:10px;">
						<td><?php echo date('d M, Y', strtotime($pen['tgl'])); ?></td>
						<td></td>
					</tr>
					<tr>
						<td colspan="2"><span style="font-weight: bold;font-size: 16px;">株式会社サリラヤ</span></td>
					</tr>
					<tr>
						<td colspan="2">445－0802 愛知県西尾市米津町北浦75-1  </td>
					</tr>
					<tr>
						<td colspan="2">ＴＥＬ．　0563－54－0372  </td>
					</tr>
					<tr>
						<td colspan="2">ＦＡＸ．　0563－77－0904  </td>
					</tr>
					<tr>
						<td colspan="2">ＨＰ．090－1784－4264ＳＢ　  </td>
					</tr>
				</table>
			</div>
			<div id="content" style="margin-top:10px;">
				<table class="tb_result" width="100%">
					<tr >
						<td colspan="6" style="background-color:#ffc000;font-size:15px;border:none;">毎度ありがとうございます。下記のとおりご請求申しあげます。</td>
					</tr>
					<tr>
						<th>No.</th>
						<th width="10%">Merk</th>
						<th>Product Name</th>
						<th>Quantity</th>
						<th>Price</th>
						<th>Amount</th>
					</tr>
					<?php
					$no=1;
					$amount = 0;
					 foreach ($detail as $row) {
					 	$amount += $row['total'];
						?>

						<tr>
							<td align="center"><?php echo $no; ?></td>
							<td><?php echo $row['merek']; ?></td>
							<td><?php echo $row['nama']; ?></td>
							<td align="center"><?php echo $row['qty']; ?></td>
							<td id="td_price_1">&#165;<?php echo $row['harga']; ?></td>
							<td id="td_amount_1">&#165;<?php echo $row['total']; ?></td>
						</tr>

						<?php
						$no++;
					} ?>

					<tr>
						<td style="background-color:#fcd663;"></td>
						<td style="background-color:#fcd663;"></td>
						<td style="background-color:#fcd663;"></td>
						<td style="background-color:#fcd663;"></td>
						<td style="background-color:#fcd663;">Amount</td>
						<td style="background-color:#fcd663;font-weight:bold;" id="total">&#165;<?php echo $amount; ?></td>
					</tr>

					<!-- <tr>
						<td></td>
						<td></td>
						<td>Cash On Delivery fees（代金引手数料）</td>
						<td></td>
						<td></td>
						<td id="cash_on_delivery_fees"><script> $("#cash_on_delivery_fees").html("&yen "+numberFormat("0"));</script></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td>Consumtion TAX　消費税 (0%)</td>
						<td></td>
						<td></td>
						<td id="consumtion_tax"><script> $("#consumtion_tax").html("&yen "+numberFormat("0"));</script></td>
					</tr> -->
					<tr>
						<td></td>
						<td></td>
						<td>Delivery Charges　（送料）</td>
						<td></td>
						<td></td>
						<td id="delivery_charges">&#165;<?php echo decimals($pen['ongkir']); ?></td>
					</tr>
					<!-- <tr>
						<td></td>
						<td></td>
						<td>Dicount 　（割引）</td>
						<td></td>
						<td></td>
						<td id="discount"><script> $("#discount").html("&yen "+numberFormat("0"));</script></td>
					</tr> -->

					<!-- GRAND TOTAL-->
					<tr>
						<td style="background-color:#ffc000;"></td>
						<td style="background-color:#ffc000;"></td>
						<td style="background-color:#ffc000;"></td>
						<td style="background-color:#ffc000;font-weight:bold;" colspan="2">GRAND TOTAL</td>
						<td style="background-color:#ffc000;font-weight:bold;" id="grand_total">&#165;<?php echo decimals((int)$pen['sub_total'] + (int)$pen['ongkir']); ?></td>
					</tr>

				</table>
			</div>
			<div id="footer">
				<table width="100%">
					<tr >
						<td>毎度ありがとうございます。下記のとおりご請求申しあげます。</td>
					</tr>
					<tr>
						<td>ただし、ゆうちょ銀行口座からお振込の場合は【記号12100－　番号81503021】</td>
					</tr>
					<!-- <tr>
						<td colspan="2">Transfer  Japan Post	: ACC Number: 12100-81503021 *ACC Name : 株）SARIRAYA*</td>
					</tr>
					<tr>
						<td colspan="2">PENGEMAS : </td>
					</tr>
					<tr>
						<td colspan="2">Deliery On (時間 帯 指定) : 2020-09-01 / 09 - 12</td>
					</tr>
					<tr>
						<td colspan="2">Notes : </td>
					</tr> -->
				</table>
			</div>
		</div>
	</body>
	</html>

