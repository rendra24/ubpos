<div class="main-panel">
  <div class="content-wrapper">

    <div class="row">
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">

 <div class="row">
              <div class="col-lg-6">
                <h4 class="card-title">Pengaturan Akun</h4>
              </div>
              <div class="col-lg-6" style="text-align: right;">
               <button type="submit" class="btn btn-success btn-fw">Save</button>
             </div>
           </div>

            <div class="row">
              <div class="col-lg-3">
                <p>Basic Information</p>
              </div>
              <div class="col-lg-9">
                <div class="row">
                  <div class="col-lg-3">
                    <div style="width: 100%;background: gray;height: 150px;">

                    </div>
                  </div>
                  <div class="col-lg-9">
                    <?php 
                      $row = $this->db->get('setting')->row_array();
                     ?>
                    <div class="form-group">
                      <label>Nama Perusahaan</label>
                      <input type="text" name="nama_usaha" class="form-control input-sm" value="<?php echo $row['nama_usaha']; ?>">
                    </div>

                    <div class="form-group">
                      <label>Nama Owner</label>
                      <input type="text" name="nama" class="form-control input-sm" value="<?php echo $row['nama']; ?>">
                    </div>

                    <div class="form-group">
                      <label>Email</label>
                      <input type="text" name="email" class="form-control input-sm" value="<?php echo $row['email']; ?>">
                    </div>

                    <div class="form-group">
                      <label>Phone</label>
                      <input type="text" name="phone" class="form-control input-sm" value="<?php echo $row['phone']; ?>">
                    </div>

                    <div class="form-group">
                      <label>Alamat</label>
                      <textarea class="form-control input-sm" name="alamat"><?php echo $row['alamat']; ?></textarea>
                    </div>

                  </div>
                </div>

              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>

  </div>
</div>


