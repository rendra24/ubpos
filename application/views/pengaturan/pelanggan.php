<style type="text/css">
thead tr th:last-child
{
    text-align: left;
}
tbody tr td:last-child
{
    text-align: center;
}

</style>
<div class="main-panel">
  <div class="content-wrapper">


    <div class="row">

      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">

            <div class="row">
              <div class="col-lg-6">
                <h4 class="card-title">Pelanggan</h4>
             </div>
             <div class="col-lg-6" style="text-align: right;">
              <a href="<?php echo base_url().'pengaturan/form_pelanggan' ?>" class="btn btn-success btn-fw">Tambah</a>
          </div>
      </div>


      <div class="table-responsive">
        <table class="table table-hover datatables">
            <thead>
                <tr>
                    <th>#</th>
                    <th width="11%;">Nama</th>
                    <th >City</th>
                    <th>Telephone</th>
                    <th>Alamat</th>  
                    <th>Action</th>

                </tr>
            </thead>
            <tbody>
                <?php $no =1; foreach ($pelanggan as $row) {

                    ?>
                    <tr>
                        <td scope="row"><?php echo $no; ?></td>
                        <td><?php echo $row['nama']; ?></td>
                        <td><?php echo $row['kota']; ?></td>
                        <td><?php echo $row['tlp']; ?></td>
                        <td ><?php echo $row['alamat']; ?></td>

                        <td style="text-align: center;"><a href="<?php echo base_url().'pengaturan/form_pelanggan/'.$row['id']; ?>" class="btn btn-primary"><i class="mdi mdi-pencil-circle"></i>Edit</a></td>

                    </tr>
                    <?php
                    $no++;
                } ?>
            </tbody>
        </table>
    </div>
</div>
</div>
</div>

</div>
</div>
</div>


<script type="text/javascript">
    $('.hapus').click(function(event){
        event.preventDefault();
        var id_barang = $(this).attr('key');


        swal({
          title: "Apakah kamu yakin ?",
          text: "hapus data barang ini",
          icon: "warning",
          buttons: true,
          dangerMode: true,
      })
        .then((willDelete) => {
          if (willDelete) {

              $.ajax({
                type: "POST",
                url: "<?php echo base_url().'admin/remove_barang'; ?>",
                data: {id_barang:id_barang},
                success: function(data){

                    if(data == 1)
                    {
                     swal("Success!","Berhasil hapus barang.", "success")
                     .then((value) => {
                      location.reload();
                  });
                 }

             }
         });


          } else {

          }
      });




    });
</script>
<script type="text/javascript">

  $(document).ready(function(){
    $('.datatables').DataTable();
  });

</script>

