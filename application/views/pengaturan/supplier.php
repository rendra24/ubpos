 <div class="main-panel">
  <div class="content-wrapper">


<div class="row">

  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">

        <div class="row">
          <div class="col-lg-6">
           <h4 class="card-title">Supplier</h4>
         </div>
         <div class="col-lg-6" style="text-align: right;">
          <a href="<?php echo base_url().'inventori/form_supplier' ?>" class="btn btn-success btn-fw">Tambah</a>
        </div>
      </div>


      <div class="table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th> No </th>
              <th> Supplier </th>
              <th> Alamat </th>
              <th> No Tlp </th>
              <th> Status </th>
              <th> Action </th>
            </tr>
          </thead>
          <tbody>
            <?php 
            $no=1;
            foreach ($supplier as $row) {
              if($row['status'] == 1){
                $badge = '<span class="badge badge-primary">Aktif</span>';
                $act = 'Non aktif';
              }else{
                $badge = '<span class="badge badge-danger">Non Aktif</span>';
                $act = 'Aktif';
              }

              ?>
              <tr>
                <td class="py-1">
                  <?php echo $no; ?>
                </td>
                <td> <?php echo $row['nama']; ?> </td>
                <td> <?php echo $row['alamat']; ?></td>
                <td> <?php echo $row['tlp']; ?> </td>
                <td> <?php echo $badge; ?> </td>
                <td> 
                  <div class="btn-group dropdown">
                    <button type="button" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Action
                    </button>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="<?php echo base_url().'inventori/form_supplier/'.$row['id']; ?>"><i class="fa fa-reply fa-fw"></i>Edit</a>
                      <a class="dropdown-item hapus" style="cursor: pointer;" key="<?php echo $row['id']; ?>"><i class="fa fa-history fa-fw"></i><?php echo $act; ?></a>

                    </div>
                  </div>
                </td>
              </tr>
              <?php
              $no++;
            }
            ?>


          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

</div>
</div>
</div>


<script type="text/javascript">
  $('.hapus').click(function(){
    var id = $(this).attr('key');

    $.ajax({
      type: "POST",
      url: "<?php echo base_url().'inventori/act_supplier'; ?>",
      data: {id:id},
      success: function(data) {
        if(data == 1)
        {
          location.reload();
        }

      }
    });

  });
</script>