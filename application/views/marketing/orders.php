 <style type="text/css">
       .card-active{
    border-bottom: 3px solid #308ee0;
  }
  .icon-blue{
    color: #308ee0;
  }
  .icon-grey{
    color: #DDD;
  }
  .color-grey{
    color: #DDD;
  }
  .bill-body{
        padding: 25px;
    padding-top: 20px;
    background: #fff;
    box-shadow: 0 6px 12px 0 rgba(0,0,0,.07);

  }

  .bill-body table{
        font-size: 14px;
    color: #6b777e;
  }

  .bill-body table td{
       padding-bottom: 10px;
  }
 </style>
 <div class="main-panel">
  <div class="content-wrapper">


<div class="row">

  <div class="col-lg-12 grid-margin stretch-card">

    <div class="card">
      <div class="card-body">
        <form action="" method="POST">
          <div class="row">
            <div class="col-md-3">
              <label>First Date</label>
              <input type="date" name="tgl_awal" class="form-control input-sm" value="<?php if(isset($tgl_awal)){ echo $tgl_awal; } ?>" >
            </div>
             <div class="col-md-3">
              <label>Last Date</label>
              <input type="date" name="tgl_akhir" class="form-control input-sm" value="<?php if(isset($tgl_akhir)){ echo $tgl_akhir; } ?>">
            </div>
           
            <div class="col-md-3">

              <button type="submit" class="btn btn-info btn-sm" style="margin-top: 35px;"><i class="mdi mdi-search-web"></i> Find</button>
            </div>
          </div>
        </form>
      </div>
    </div>

  </div>


    <div class="col-lg-12 grid-margin stretch-card">

    <div class="card">
      <div class="card-body">

        <div class="row">
          <div class="col-lg-6">
           <h4 class="card-title">Sales Orders</h4>
         </div>
         <div class="col-lg-6" style="text-align: right;">
          
        </div>
      </div>


      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th> Date </th>
              <th> Product </th>
              <th> Total (Rp) </th>
              <th> Status </th>
              <th> Tracking Number </th>
              <th> Action </th>
            </tr>
          </thead>
          <tbody>
            <?php 
            foreach ($penjualan as $row) {

                if($row['status'] == 1){
                    $status = '<span class="badge badge-warning">Unpaid</span>';
                  }else if($row['status'] == 2){
                    $status = '<span class="badge badge-success">Paid</span>';
                  }else if($row['status'] == 3){
                   $status = '<span class="badge badge-primary">In Delivery</span>';
                 }else if($row['status'] == 5){
                   $status = '<span class="badge badge-danger">Cancel</span>';
                 }else{
                   $status = '<span class="badge badge-warning">Unpaid</span>';
                 }

              if($row['no_resi'] == ''){
                $resi = '-';
              }else{
                $resi = $row['no_resi'];
              }
            

              ?>
              <tr style="cursor: pointer;" class="action-bill" >
                <td> <?php echo $row['tgl']; ?> </td>
                 <td> <?php echo $row['produk']; ?></td>
                <td> <?php echo decimals($row['sub_total']); ?> </td>
                <td><?php echo $status; ?></td>
                <td><?php echo $resi; ?></td>
                <td>
                  <a href="<?php echo base_url().'penjualan/orders/'.$row['id']; ?>" class="action-bill btn btn-primary"><i class="mdi mdi-eye"></i></a>
                  <?php if($row['bukti'] == ''){ ?>
                  <button type="button" class="btn btn-info btn-sm btn-confrim" key="<?php echo $row['id']; ?>">Payment Confirmation</button>
                <?php } ?>
                </td>
              </tr>
              <?php
            }
            ?>


          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

</div>
</div>
</div>


<?php 
if($id_penjualan != ''){
 $penjualan_detail = $this->db->query("SELECT c.nama , b.qty , b.total from penjualan a 
            LEFT JOIN penjualan_detail b ON a.id = b.id_penjualan
            LEFT JOIN produk c ON b.id_produk = c.id 
            WHERE a.id = ".$id_penjualan)->result_array();


        //penjualan
        $penjualan = $this->db->query("SELECT * FROM penjualan WHERE id=".$id_penjualan)->row_array();

        $setting = $this->db->get('setting')->row_array();
  
 ?>
<div class="row div-bill" style="position: fixed;height: 100%;width: 101%;top: 0;right: 0;left: 0;bottom: 0;z-index: 9999;display: none;">
  <div class="col-md-8" style="background-color: #000000b5;">
    
  </div>
  <div class="col-md-4" style="background-color: #f3f3f3;">
    <div style="padding-top: 15px;"><div style="text-align: right;font-size: 16px;cursor: pointer;" class="close-bill">X</div></div>
    <div class="bill-body">
      <div style="font-size: 20px;
    color: #6b777e;
    font-weight: 700;
    text-align: center;"><?php echo $setting['nama']; ?></div>
    <p style="text-align: center;color: #6b777e;margin-bottom: 0px;"><?php echo $setting['alamat']; ?></p>
    
      <div style="border-bottom: 2px dashed #aaa;
    margin: 20px 0;"></div>

    <table style="width: 100%;">
      <?php foreach ($penjualan_detail as $row) { ?>
        <tr>
          <td><?php echo $row['nama']; ?></td>
          <td>x <?php echo $row['qty']; ?></td>
          <td style="text-align: right;"><?php echo decimals($row['total']); ?></td>
        </tr>
        <?php } ?>
      </table>

      <div style="border-bottom: 2px dashed #aaa;
    margin: 20px 0;"></div>

     <table style="width: 100%;">
        <tr>
          <td>Sub Total</td>
          
          <td style="text-align: right;"><?php echo decimals($penjualan['sub_total']); ?></td>
        </tr>
        <tr>
          <td>Postage</td>
         
          <td style="text-align: right;"><?php echo decimals($penjualan['ongkir']); ?></td>
        </tr>
        <tr>
          <td>Total</td>
          
          <td style="text-align: right;"><?php echo decimals($penjualan['sub_total'] + $penjualan['ongkir']); ?></td>
        </tr>
      </table>

       <div style="border-bottom: 2px dashed #aaa;
    margin: 20px 0;"></div>

     <table style="width: 100%;">
        <tr>
          <td>Nominal Payment</td>
          
          <td style="text-align: right;"><?php echo decimals($penjualan['bayar']); ?></td>
        </tr>
        <tr>
          <td>Change Money</td>
         
          <td style="text-align: right;"><?php echo decimals($penjualan['kembali']); ?></td>
        </tr>
      </table>


    </div>
  </div>
</div>
<?php } ?>


<div class="modal fade" id="buktimdl" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Upload Evidence of transfer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="form-upload">
      <div class="modal-body">
        <div class="form-group">
          <label>Evidence of transfer</label>
          <input type="file" name="bukti" class="form-control input-sm bukti">
          <input type="hidden" name="id_penjualan" class="id_penjualan">
        </div>
        <img src="" style="display: none;width: 100%;height: auto;" id="image-upload">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary btn-submit">Upload</button>
      </div>
      </form>
    </div>
  </div>
</div>


<script type="text/javascript">
 

  $(document).ready(function(){
    <?php if($id_penjualan != ''){ ?>
      $('.div-bill').show();
    <?php } ?>
  });

  $('.close-bill').click(function(){
    $('.div-bill').hide();
  });

  $('.btn-confrim').click(function(){
    $('#buktimdl').modal('show');

    var id_penjualan = $(this).attr('key');
    $('.id_penjualan').val(id_penjualan);
  });


  function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#image-upload').attr('src', e.target.result).show();
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

$(".bukti").change(function() {
  readURL(this);
});


$(".form-upload").submit(function(event){
  event.preventDefault();

  var formData = new FormData(this);

  $.ajax({
    type: "POST",
    url: "<?php echo base_url().'penjualan/upload_bukti'; ?>",
    data:formData,
    processData:false,
    contentType:false,
    cache:false,
    async:true,
    beforeSend: function() {
      $('.btn-submit').prop('disabled', true);
      $('.btn-submit').removeClass('btn-primary').addClass('btn-secondary').text('Loading');
    },
    success: function(data) {

      if(data == 1)
      {
        window.location = '<?php echo base_url().'penjualan/orders'; ?>';
      }
    }
  });

});
</script>