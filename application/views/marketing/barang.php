 <style type="text/css">
   .card-active{
    border-bottom: 3px solid #308ee0;
  }
  .icon-blue{
    color: #308ee0;
  }
  .icon-grey{
    color: #DDD;
  }
  .color-grey{
    color: #DDD;
  }

  .table td img, .table th img {
    border-radius: 0%;
  }
</style>
<div class="main-panel">
  <div class="content-wrapper">
    <div class="row">

      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">

            <div class="row">
              <div class="col-lg-6">
               <h4 class="card-title">Products</h4>
             </div>
             <div class="col-lg-6" style="text-align: right;">
              
            </div>
          </div>


          <div class="table-responsive">
            <table class="table table-striped table-data">
              <thead>
                <tr>
                  <th> Products </th>
                  <th> Category </th>
                  <th> Brand </th>
                  <th> Price Marketing (Rp) </th>
                </tr>
              </thead>
              <tbody>


              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
</div>




<script type="text/javascript" src="<?php echo base_url('datatables/datatables.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('datatables/lib/js/dataTables.bootstrap.min.js') ?>"></script>
<script>
  var tabel = null;

  $(document).ready(function() {

    fecthing_data();
    function fecthing_data(){


      tabel = $('.table-data').DataTable({
        "processing": true,
        "serverSide": true,
            "ordering": true, // Set true agar bisa di sorting
            "order": [[ 0, 'asc' ]], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
            "ajax":
            {
                "url": "<?php echo base_url('admin/barang_view') ?>", // URL file untuk proses select datanya
                "type": "POST"
              },
              "deferRender": true,
              "aLengthMenu": [[10, 50, 100],[ 10, 50, 100]],
              "columns": [
              { "data": "nama" },
              { "data": "nama_kategori" },
              { "data": "merek" },
              { "data": "harga" }, 
          ],
        });

    }

    $( document ).on("click", ".hapus", function(event){

      event.preventDefault()
      var id = $(this).attr('key');

      $.ajax({
        type: "POST",
        url: "<?php echo base_url().'admin/remove_barang'; ?>",
        data: {id:id},
        success: function(data) {
          if(data == 1)
          {
            $('.table-data').DataTable().destroy();
            fecthing_data();
          }

        }
      });
    });

  });


  $('.form-excel').submit(function(event){
    event.preventDefault();
    var formData = new FormData(this);

  $.ajax({
    type: "POST",
    url: "<?php echo base_url().'import/upload'; ?>",
    data:formData,
    processData:false,
    contentType:false,
    cache:false,
    async:false,
    beforeSend: function() {
      $('.btn-upload').prop('disabled', true);
      $('.btn-upload').removeClass('btn-primary').addClass('btn-secondary').text('Loading');
    },
    success: function(data) {
      swal({
      icon: "success",
      text: "Succes import products",
    });
      $('.btn-upload').prop('disabled', false);
      $('.btn-upload').removeClass('btn-secondary').addClass('btn-primary').text('Upload Excel');
    
    }
  });
  });
</script>