<style type="text/css">
	@font-face {
  font-family: MonospaceTypewriter;
  src: url(<?php echo base_url().'assets/fonts/MonospaceTypewriter.ttf' ?>);
}

table {
  font-family: MonospaceTypewriter;
  font-size: 9px;
}
td {
	margin-left: 0px;
	margin-right: 0px;
	padding-left: 0px;
	padding-right: 0px;
}
</style>

<table style="width: 100%;margin: 0px;margin-bottom: 20px;">
	<tr>
		<td>Nomor Nota</td>
		<td style="text-align: right;padding-right: 10px;">BK0001456789</td>
	</tr>
	<tr>
		<td>Waktu</td>
		<td style="text-align: right;padding-right: 10px;">11-09-2020 17:24</td>
	</tr>
	<tr>
		<td>Kasir</td>
		<td style="text-align: right;padding-right: 10px;">Rendra Sukun</td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-top: 1px dotted #000;"></td>
	</tr>
	<tr>
		<td>Kentang Goreng</td>
		<td style="text-align: right;padding-right: 10px;">20.000</td>
	</tr>
		<tr>
		<td>Kapiten</td>
		<td style="text-align: right;padding-right: 10px;">20.000</td>
	</tr>
		<tr>
		<td>Kopi Susu</td>
		<td style="text-align: right;padding-right: 10px;">20.000</td>
	</tr>
		<tr>
		<td>Samyang Bungkus</td>
		<td style="text-align: right;padding-right: 10px;">20.000</td>
	</tr>
		<tr>
		<td>Pepsoden</td>
		<td style="text-align: right;padding-right: 10px;">20.000</td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-top: 1px dotted #000;"></td>
	</tr>
	<tr>
		<td>Subtotal</td>
		<td style="text-align: right;padding-right: 10px;">100.000</td>
	</tr>
	<tr>
		<td>Total Tagihan</td>
		<td style="text-align: right;padding-right: 10px;">100.000</td>
	</tr>
	<tr>
		<td colspan="2"><hr style="border-top: 1px dotted #000;"></td>
	</tr>
	<tr>
		<td>Tunai</td>
		<td style="text-align: right;padding-right: 10px;">100.000</td>
	</tr>
	<tr>
		<td>Total Bayar</td>
		<td style="text-align: right;padding-right: 10px;">100.000</td>
	</tr>
	<tr>
		<td>Kembalian</td>
		<td style="text-align: right;padding-right: 10px;">0</td>
	</tr>

</table>

<script type="text/javascript">
        function PrintWindow()
        {                    
           window.print();            
           CheckWindowState();
        }
       
        function CheckWindowState()
        {           
            if(document.readyState=="complete")
            {
                window.close(); 
            }
            else
            {           
                setTimeout("CheckWindowState()", 2000)
            }
        }    
        
       PrintWindow();
</script>