<style type="text/css">
    thead tr th:last-child
    {
        text-align: left;
    }
     tbody tr td:last-child
    {
        text-align: left;
    }
</style>
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">KAS OFFICE</h3> </div>
           <!--  <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div> -->
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-12">
                     <button type="button" class="btn btn-info btn-flat btn-addon m-b-10 m-l-5 " onclick="location.href='<?php echo base_url().'admin/form_kas'; ?>'"><i class="ti-plus"></i>Tambah KAS</button> 
                   <div class="card">
                    <div class="card-title">
                        <h4>List KAS </h4>

                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>KAS</th>
                                        <th>Tanggal</th>
                                        <th>Masuk</th>
                                        <th>Keluar</th>
                                        <th>Keterangan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $no =1;
                                    foreach ($kas as $row) {
                                        ?>
                                        <tr>
                                            <th scope="row"><?php echo $no; ?></th>
                                            <td><?php echo $row['nama_kas']; ?></td>
                                            <td><?php echo $row['waktu']; ?></td>     
                                            <td><?php echo $row['masuk']; ?></td> 
                                            <td><?php echo $row['keluar']; ?></td>   
                                            <td><?php echo $row['keterangan']; ?></td>                             
                                           <!--  <td style="text-align: center;"><a href="" class="btn btn-primary">Edit</a></td>
                                             <td><a href="" class="btn btn-danger">Hapus</a></td> -->
                                        </tr>
                                        <?php
                                        $no++;
                                    } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->


