
<style type="text/css">

.border-aktive{
	border: 2px solid #FFEB3B;
}
</style>
<style type="text/css">
.container-fluid{
		padding-left: 80px;
		padding-right: 80px;
	}
	.card-dekstop{
		display: block;
	}


.modal-dialog{
    overflow-y: initial !important
}
.modal-body{
	height: auto;
    overflow-y: auto;
}


	@media only screen and (max-width: 600px) {
		.container-fluid{
		padding-left: 0px;
		padding-right: 0px;
		padding-top: 83px;
		padding-bottom: 60px;
	}

		.row{
		margin-right: 0px;
		margin-left: 0px;
		padding: 5px;
	}
	.card-body{
		padding: 8px;
	}
	.card-dekstop{
		display: none;
	}
	}
</style>
<?php 
		$kode_transaksi = $this->session->userdata('kode_transaksi');
		$id_user = $this->session->userdata('id_user');
				$out = $this->db->get_where("transaksi_temp", array('id_user' => $id_user, 'id' => $kode_transaksi))->row_array();
 ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-7">
			<div class="d-flex area-pengiriman" style="width: 100%;padding: 10px 15px;border-radius: 4px;margin-bottom: 15px;">
				<div>
					Area Pengiriman <br>
					<select class="form-control input-sm" style="font-size:12px;" name="id_cabang" id="id_cabang">
						<option value="0">-- Pilih Area --</option>
				
					<?php   $id = $this->session->userdata('id_cabang');
  $get = $this->db->query("SELECT id,nama_kantor FROM cabang  WHERE flag=1 AND kota=(SELECT kota FROM cabang WHERE id='".$id."') ")->result_array();
  foreach ($get as $row) { ?>
  	<option value="<?php echo $row['id']; ?>"><?php echo $row['nama_kantor']; ?></option>
  <?php } ?>
  	</select>
				</div>
				<div class="ml-auto" style="padding-top: 12px;">
					<i class="fa fa-map-marker" aria-hidden="true" style="font-size: 22px;"></i>
				</div>	
			</div>

			<div class="card" style="margin-bottom: 15px;">
				<div class="card-body" style="padding: 15px;">
					<div class="d-flex" >
				<div id="alamat_terpilih">

				
				</div>
				<div class="ml-auto"><a href="#" class="btn btn-info btn-sm pilih-alamat" data-toggle="modal" data-target="#alamatMdl">Pilih alamat</a></div>	
			</div>

			<div class="d-flex" style="margin-top: 15px;">
				<div style="font-size: 14px;">Pilih Tanggal Pengiriman</div>
				<div class="ml-auto">
					<select class="form-control pilih-tgl input-sm" style="font-size: 12px;">
						<option value="0">-- Pilih Tanggal Kirim --</option>
						<?php 
						$tomorrow = date('Y-m-d',strtotime(date('Y-m-d') . "+1 days"));
						$tgl_besok = date('Y-m-d');
						foreach ($tgl_pengiriman as $row) { 

							if($row != 'tidak aktif'){
							if(date('Y-m-d') != $row && date('Y-m-d') < $row){
							
							if(date('H:i:s') >= '16:00:00'){
								if($tomorrow == $row){
									$tgl_besok = $row;
								}
							}

							if($tgl_besok != $row && $tgl_besok != ''){

							?>
							<option value="<?php echo $row; ?>" <?php if($out['tgl_pengiriman'] == $row){ echo "selected"; } ?> ><?php echo date_indo($row); ?></option>
						<?php
							}
							}
							}
						 } 
						 ?>
						
					</select>
				</div>
			</div>
				</div>
			</div>

			

					<div class="row" style="margin: 0px;">
						<div class="col-md-12" style="margin-bottom: 15px;padding:15px;padding-bottom:0px;background-color: #f3f4fa;">
								<?php 
						$id_user = $this->session->userdata('id_user');
						$kode_transaksi = $this->session->userdata('kode_transaksi');
						$query_p = "SELECT a.*, b.nama_produk , d.nama_satuan from transaksi_temp_detail as a 
						LEFT JOIN transaksi_temp c ON a.id_transaksi = c.id
						LEFT JOIN produk as b ON a.id_produk = b.id 
						LEFT JOIN satuan as d ON b.id_satuan = d.id
						WHERE a.id_user='$id_user' AND c.id = '$kode_transaksi' GROUP BY a.id_produk";
						$shop = $this->db->query($query_p);

						if($shop->num_rows() > 0){
							$shop = $shop->result_array(); 
							$grand_total = 0;

						foreach ($shop as $row) {
							$grand_total += $row['sub_total'];
							?>
							<div class="card" style="margin-bottom: 15px;">
								<div class="card-body" style="padding: 12px;padding-bottom: 0px;">

									<div class="row d-flex" style="margin: 0px;margin-bottom: 20px;">
										<div>
										<p style="font-size: 12px;font-weight: 500;margin-bottom: 0px;"><?php echo $row['nama_produk']; ?></p>
										<p style="color: #9295a6;font-size: 12px;margin-bottom: 0px;"><?php echo rupiah($row['harga']); ?> / <?php echo $row['nama_satuan']; ?></p>		
									</div>
									<div class="ml-auto">
										<!-- <i class="fa fa-trash-o" aria-hidden="true"></i> -->									
									</div>
									</div>

									<div class="row d-flex" style="margin: 0px;">
										<div>
											<p>Qty <?php echo $row['qty']; ?></p>
										</div>
										<div class="ml-auto">
											<p style="font-size: 12px;font-weight: 500;margin-bottom: 0px;"><?php echo rupiah($row['sub_total']); ?></p>
										</div>
									</div>
									
								</div>
							</div>
								<?php } ?>

						

							<?php
						}else{
							?>
							<div style="padding: 50px 25px;text-align: center;">
								
							
								<img src="<?php echo base_url().'assets/empty.svg' ?>" style="margin-bottom: 10px;height: 30px;">
									<p style="font-weight: 700;font-size: 12px;margin-bottom: 0px;">Belanja dan buat pesanan sekarang!</p>
									<p style="font-size: 12px;margin-bottom: 10px;">Temukan produk dengan harga yang kompetitif dan membayar harga yang adil bagi para petani.</p>

							</div>

							<?php
						}

						
						?>


						</div>

					</div>

					
				
			
		</div>

		<div class="col-md-4">
			<?php 
		

				
					// if($out['grand_total'] >= 100000){
					// 	$ongkir = 0;
					// 	$text = '<del>Rp 20.000</del>';
					// }else{
					// 	$ongkir = 20000;
					// 	$text = '';
					// }

					  if($out['grand_total'] >= 100000 && $out['grand_total'] < 150000){
			                $ongkir = 15000;
						$text = '<del>Rp 20.000</del>';
			            }elseif($out['grand_total'] >= 150000){
			                $ongkir = 0;
							$text = '<del>Rp 20.000</del>';
			            }else{
			                $ongkir = 20000;
			                $text = '';
			            }
				


				$total_all = $ongkir + $out['grand_total'];

			 ?>
			<div class="card">
				<div class="card-body">
					<p style="font-size: 14px;">Ringkasan</p>

					<div class="card" style="margin-bottom: 15px;">
						<div class="card-body row" style="padding: 8px;">
							<div class="col-md-1">
								<img src="https://tanihub.com/static/img/voucher.png">
							</div>
							<div class="col-md-11">
								<p style="margin-bottom: 0px;font-weight: 700;font-size: 12px;">Gunakan Voucher</p>
							<p style="margin-bottom: 0px;font-size: 10px;">Pilih Voucher untuk mendapatkan potongan</p>
							</div>
							
						</div>
					</div>

					<div style="margin-bottom: 15px;">
					<div class="d-flex" style="border-bottom: 1px solid #DDD;padding: 8px;">
						<div class="text-comp">Total Harga</div>
						<div class="ml-auto text-comp"><b><?php echo rupiah($out['grand_total']); ?></b></div>
					</div>
					<div class="d-flex" style="border-bottom: 1px solid #DDD;padding: 8px;">
						<div class="text-comp">Voucher</div>
						<div class="ml-auto text-comp"><b>Rp 0</b></div>
					</div>
					<div class="d-flex" style="border-bottom: 1px solid #DDD;padding: 8px;">
						<div class="text-comp">Ongkos Kirim</div>
						<div class="ml-auto text-comp"><b class=""><?php echo $text." ".rupiah($ongkir); ?></b></div>
					</div>
					<div class="d-flex" style="border-bottom: 1px solid #DDD;padding: 8px;">
						<div class="text-comp">Grand Total</div>
						<div class="ml-auto text-comp is-primary"><b><?php echo rupiah($total_all); ?></b></div>
					</div>
					</div>

					<p style="font-size: 14px;">Transfer Pembayaran (Pilih metode pembayaran)</p>
					<?php 
						$bank = $this->db->get_where('data_bank', array('flag' => '1'))->result_array();
						foreach ($bank as $row) {
					?>
					<div class="card payment-card pilihan-payment" key="<?php echo $row['id']; ?>" style="margin-bottom: 15px;">
						<div class="card-body" style="padding: 8px;">
							<p class="text-comp" style="margin-bottom: 0px;"><b><?php echo $row['nama_bank']; ?></b></p>
							<p class="text-comp" style="margin-bottom: 0px;"><?php echo $row['atas_nama']; ?> , <?php echo $row['no_rekening']; ?></p>
						</div>
					</div>
				<?php } ?>

					
					<?php if($out['id_pengiriman'] == 0){ ?>
					<div class="alert alert-danger alert-alamat" role="alert" style="padding: 8px;margin-bottom: 15px;font-size: 12px;">
					  Alamat pengiriman belum dipilih
					</div>
					<?php } ?>

					<?php if($out['tgl_pengiriman'] == 0){ ?>
					<div class="alert alert-danger alert-pengiriman" role="alert" style="padding: 8px;margin-bottom: 15px;font-size: 12px;">
					  Tanggal pengiriman belum dipilih
					</div>
					<?php } ?>


					<div class="alert alert-danger alert-cabang" role="alert" style="padding: 8px;margin-bottom: 15px;font-size: 12px;">
					  Area pengiriman belum dipilih
					</div>
					

					
					<input type="hidden" name="id_bank" id="id_bank" value="0">
					<input type="hidden" name="id_transaksi" id="id_transaksi" value="<?php echo $out['id']; ?>">

					<button class="btn btn-success btn-block btn-bayar" disabled>Bayar <?php echo rupiah($total_all); ?></button>
				</div>
			</div>
		</div>
	</div>
</div>



<div class="modal fade" id="alamatMdl" tabindex="-1" role="dialog" aria-labelledby="alamatMdlLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="margin-top: 150px;">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title" id="alamatMdlLabel">Pilih Alamat Pengiriman</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<a href="#" id="add_alamat" class="btn btn-success btn-block">Tambah Alamat Baru</a>
      	<div id="load_alamat">
      		
      	</div>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="newalamat" tabindex="-1" role="dialog" aria-labelledby="alamatMdlLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title" id="alamatMdlLabel">Pilih Alamat Pengiriman</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="height: 600px;">
      	<form action="<?php echo base_url().'web/save_alamat'; ?>" method="post" id="form_alamat">
      		
      
      	<div class="row">
      		<div class="col-md-6">
      		

      		<label>Nama Penerima</label>
      		<input type="text" name="nama" class="form-control input-sm">

      		<label>Nomor Handphone</label>
      		<input type="text" name="telephone" class="form-control input-sm">

      		<label>Provinsi</label>
      		<select class="form-control input-sm id_prov"  name="id_provinsi">
      			<option>-- Pilih Provinsi --</option>
      			<?php foreach ($provinsi as $row) { ?>
      					<option value="<?php echo $row['id_prov']; ?>"><?php echo $row['nama']; ?></option>
      			<?php } ?>
      			
      		</select>

      		<label>Kota</label>
      		<select class="form-control input-sm id_kota" name="id_kota">
      			<option></option>
      		</select>
      		</div>

      		<div class="col-md-6">
      			<label>Kecamatan</label>
      		<select class="form-control input-sm id_kecamatan" name="id_kecamatan">
      			<option></option>
      		</select>

      		<label>Kelurahan</label>
      		<select class="form-control input-sm id_kelurahan" name="id_kelurahan">
      			<option></option>
      		</select>

      		<label>Kode Pos</label>
      		<input type="text" name="kode_pos" class="form-control input-sm">

      		<label>Alamat Lengkap</label>
      		<input type="text" name="alamat" class="form-control input-sm">


      		
      	
      	</div>

      	<div class="col-md-12">
      		<button type="submit" class="btn btn-success btn-block btn-add-alamat"  style="margin-top: 20px;">Simpan</button>
      	</div>
      	</div>
 		
	</form>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
	alamat_terpilih();

	$('#add_alamat').click(function(event){
		event.preventDefault();
		$('#alamatMdl').modal('hide');
		$('#newalamat').modal('show');
	});


	$('.pilihan-payment').click(function(){
		var id_bank = $(this).attr('key');
		$('.pilihan-payment').removeClass('border-aktive');
		$(this).addClass('border-aktive');
		$('#id_bank').val(id_bank);
		cek_bayar();
	});


	function alamat_terpilih(){

		$.ajax({
				url : "<?php echo base_url().'web/alamat_terpilih'; ?>",
				type: "POST"
			}).done(function(response){
				$('#alamat_terpilih').html(response);
				cek_bayar();
			});
		
	}

	function cek_bayar(){

		var id_pengiriman = $('#id_pengiriman').val();
		var id_bank = $('#id_bank').val();
		var tgl_pengiriman = $('.pilih-tgl').val();
		var id_cabang = $('#id_cabang').val();

		if(id_pengiriman != 0){
			$('.alert-alamat').css('display','none');
		}

		if(tgl_pengiriman != 0){
			$('.alert-pengiriman').css('display', 'none');
		}

		if(id_cabang != 0){
			$('.alert-cabang').css('display', 'none');
		}

		if(id_pengiriman != 0 && id_bank != 0 && tgl_pengiriman != 0 && id_cabang != 0){
			$('.btn-bayar').removeAttr("disabled");
		}




	}



	$('.btn-bayar').click(function(){
		$(this).removeClass('btn-success');
			$(this).addClass('btn-secondary');
			$(this).prop('disabled', true).text('Loading..');
			
		var id_bank = $('#id_bank').val();
		var id_transaksi = $('#id_transaksi').val();
		var id_cabang = $('#id_cabang').val();

			$.ajax({
				url : "<?php echo base_url().'web/update_pembayaran'; ?>",
				type: "POST",
				data : {id_bank:id_bank, id_transaksi:id_transaksi, id_cabang:id_cabang}
			}).done(function(response){
				window.location = "<?php echo base_url().'pembayaran/q/' ?>"+id_transaksi;
			});
	});



	$('.pilih-alamat').click(function(){
			var id_transaksi = $("#id_transaksi").val();
			$.ajax({
				url : "<?php echo base_url().'web/get_alamat'; ?>",
				type: "POST",
			}).done(function(response){
				$('#load_alamat').html(response);

				$('.btn-pilih').click(function(){
					var id_pengiriman = $(this).attr('key');
					$.ajax({
				url : "<?php echo base_url().'web/update_pegiriman'; ?>",
				type: "POST",
				data : {id_pengiriman:id_pengiriman, id_transaksi:id_transaksi}
			}).done(function(response){
				alamat_terpilih();
				$('#alamatMdl').modal('hide');
				$('#newalamat').modal('hide');
			});
				});


			});
	});


	
			
			$('.pilih-tgl').change(function(){
					var tgl = $(this).val();
					var id_transaksi = $("#id_transaksi").val();
					$.ajax({
				url : "<?php echo base_url().'web/update_pegiriman_tgl'; ?>",
				type: "POST",
				data : {tgl:tgl, id_transaksi:id_transaksi}
			}).done(function(response){
				cek_bayar();
			});
				});
	


	$('.id_prov').change(function(){
		var id_prov = $(this).val();

		$.ajax({
				url : "<?php echo base_url().'web/get_kota'; ?>",
				type: "POST",
				data : {id_prov:id_prov}
			}).done(function(response){
				$('.id_kota').html(response);
			});

	});

	$('.id_kota').change(function(){
		var id_kota = $(this).val();

		$.ajax({
				url : "<?php echo base_url().'web/get_kecamatan'; ?>",
				type: "POST",
				data : {id_kota:id_kota}
			}).done(function(response){
				$('.id_kecamatan').html(response);
			});

	});

	$('.id_kecamatan').change(function(){
		var id_kec = $(this).val();

		$.ajax({
				url : "<?php echo base_url().'web/get_kelurahan'; ?>",
				type: "POST",
				data : {id_kec:id_kec}
			}).done(function(response){
				$('.id_kelurahan').html(response);
			});

	});

	$('#form_alamat').submit(function(event){
		event.preventDefault();
			$('.btn-add-alamat').removeClass('btn-success');
			$('.btn-add-alamat').addClass('btn-secondary');
			$('.btn-add-alamat').prop('disabled', true).text('Loading..');
		
			var post_url = $(this).attr("action");
			var request_method = $(this).attr("method");
			var form_data = $(this).serialize();

			$.ajax({
				url : post_url,
				type: request_method,
				data : form_data
			}).done(function(response){
				$('.btn-add-alamat').removeClass('btn-secondary');
				$('.btn-add-alamat').addClass('btn-success');
				$('.btn-add-alamat').removeAttr("disabled").text('Simpan');
				$('#form_alamat .form-control');
				$('#newalamat').modal('hide');
			});
	})
</script>