<style type="text/css">
	.nav-tabs .nav-link{
		color: #9e9e9e;
	}

	.nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active{
		color: #000000;
    font-weight: 700;
    border: 0px;
    border-bottom: 2px solid #4caf50;
	}
	.nav-tabs{
		border-bottom: 1px solid #DDD;
	}

	.container-fluid{
		padding-left: 80px;
		padding-right: 80px;
	}

	.lists{
		    border-bottom: 1px solid #DDD;
    padding: 10px 0px;
	}

	.card-dekstop{
		display: block;
	}


	@media only screen and (max-width: 600px) {
		.container-fluid{
		padding-left: 0px;
		padding-right: 0px;
		padding-top: 83px;
		padding-bottom: 60px;
	}

		.row{
		margin-right: 0px;
		margin-left: 0px;
		padding: 5px;
	}
	.card-body{
		padding: 8px;
	}
	.card-dekstop{
		display: none;
	}
	}
</style>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-3 col-12 card-dekstop">
			<div class="col-md-12" style="padding: 15px;background-color: #FFF;border-radius: 4px;margin-bottom: 20px;">
					<p style="font-size: 16px;font-weight: 700;margin-bottom: 0px;"><i class="fa fa-user" style="font-size: 16px;color: #28a745;">&nbsp;&nbsp;</i> Akun Saya</p>
				</div>
				<div class="col-md-12" style="padding: 15px;background-color: #FFF;border-radius: 4px;margin-bottom: 20px;border: 1px solid green;">
					<p style="font-size: 16px;font-weight: 700;margin-bottom: 0px;"><i class="fa fa-shopping-bag" aria-hidden="true"style="font-size: 16px;color: #28a745;">&nbsp;&nbsp;</i> Pesanan Saya</p>
				</div>
		</div>
		

		<div class="col-md-9 col-12">
			<div class="card">
				<div class="card-body">
					<p style="font-weight: 700;font-size: 20px;line-height: 25px;margin-bottom: 20px;">Pesanan Saya</p>
					<nav>
						<div class="nav nav-tabs" id="nav-tab" role="tablist">
							<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Saat ini</a>
							<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Selesai</a>
							<a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Dibatalkan</a>
						</div>
					</nav>
					<div class="tab-content" id="nav-tabContent">
						<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">

							

							<?php 
							$id_user = $this->session->userdata('id_user');
								$cek_order = $this->db->query("SELECT a.*, b.nama_bank, b.no_rekening FROM transaksi_temp a JOIN data_bank b ON a.id_bank_pembayaran = b.id WHERE a.id_user='".$id_user."' AND a.status='2' ");

								if($cek_order->num_rows() > 0){
									$cek_order = $cek_order->result_array();

									foreach ($cek_order as $row) {

										$pembayaran = $row['grand_total'] + $row['ongkir'];
							?>

							<div class="card" style="margin-top: 20px;">
								<div class="card-body">
									<div class="row">
										<div class="col-md-3">
											<p class="text-comp is-grey mb5">Nomor Pesanan</p>
											<p style="font-size: 14px;font-weight: 700;margin-bottom: 5px;"><?php echo $row['kode_transaksi']; ?></p>
											<p class="text-comp is-grey"><?php echo datetime_indo($row['tgl_order']); ?></p>
										</div>
										<div class="col-md-3">
											<p class="text-comp is-grey mb5">Status Pesanan</p>
											<span style="padding: 5px;border-radius: 5px;background-color:#3fb8f4;color:#FFF;" class="text-comp">Menunggu Verifikasi Pembayaran</span>
										</div>
										<div class="col-md-3">
											<p class="text-comp is-grey mb5">Status Pesanan</p>
											<p style="font-size: 14px;font-weight: 700;margin-bottom: 5px;">BANK <?php echo $row['nama_bank']; ?></p>
											<p class="text-comp is-grey"><?php echo $row['no_rekening']; ?></p>
										</div>
											<div class="col-md-3">
											<p class="text-comp is-grey mb5">Total Pembayaran</p>
											<p style="font-size: 14px;font-weight: 700;margin-bottom: 5px;"><?php echo rupiah($pembayaran); ?></p>
										</div>
									</div>
								</div>
								<div class="card-footer">
									<a href="#" class="btn btn-primary btn-sm btn-history" key="<?php echo $row['id']; ?>" id="proses">Detail Pesanan</a>
								</div>
							</div>

							<?php
									}
								}else{
							 ?>
							

							<div class="card" style="margin-top: 20px;">
								<div class="card-body" style="text-align: center;padding: 60px 20px;">
									<img src="<?php echo base_url().'assets/empty.svg' ?>" style="margin-bottom: 10px;">
									<p style="font-weight: 700;font-size: 14px;margin-bottom: 0px;">Belanja dan buat pesanan sekarang!</p>
									<p style="font-size: 14px;margin-bottom: 10px;">Dukung petani kita dan hasil pangan lainnya dengan memilih produk lokal dalam keseharianmu bersama kami!</p>
									<a href="" class="btn btn-success btn-sm">Belanja Sekarang</a>
								</div>
							</div>

						<?php } ?>
						</div>
						<div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
							

							<?php 
							$id_user = $this->session->userdata('id_user');
								$cek_selesai = $this->db->query("SELECT a.*, DATE_FORMAT(tgl_order,'%d %M %Y') as tgl_order1, DATE_FORMAT(tgl_kirim,'%d %M %Y') as tgl_kirim1, b.nama_bank, b.no_rekening, IF(status=1,'Proses Pengiriman','Pesanan Diterima') as status_order FROM transaksi a LEFT JOIN data_bank b ON a.id_bank_pembayaran = b.id WHERE a.id_user='".$id_user."' AND a.status='1' ");

								if($cek_selesai->num_rows() > 0){
									$cek_selesai = $cek_selesai->result_array();

									foreach ($cek_selesai as $row) {

										$pembayaran = $row['grand_total'] + $row['ongkir'];
							?>

							<div class="card" style="margin-top: 20px;">
								<div class="card-body">
									<div class="row">
										<div class="col-md-3">
											<p class="text-comp is-grey mb5">Nomor Nota</p>
											<p style="font-size: 14px;font-weight: 700;margin-bottom: 5px;"><?php echo $row['no_nota']; ?></p>
											<p class="text-comp is-grey"><?php echo $row['tgl_order1']; ?></p>
										</div>
										<div class="col-md-3">
											<p class="text-comp is-grey mb5">Status Pesanan</p>
											<span style="padding: 5px;border-radius: 5px;background-color:#f4d03f;color:#FFF;" class="text-comp"><?php echo $row['status_order']; ?></span>
											<p class="text-comp is-grey"><?php echo $row['tgl_kirim1']; ?></p>
										</div>
										<div class="col-md-3">
											<p class="text-comp is-grey mb5">Transaksi</p>
											<p style="font-size: 14px;font-weight: 700;margin-bottom: 5px;">BANK <?php echo $row['nama_bank']; ?></p>
											<p class="text-comp is-grey"><?php echo $row['no_rekening']; ?></p>
										</div>
											<div class="col-md-3">
											<p class="text-comp is-grey mb5">Total Pembayaran</p>
											<p style="font-size: 14px;font-weight: 700;margin-bottom: 5px;"><?php echo rupiah($pembayaran); ?></p>
										</div>
									</div>
								</div>
								<div class="card-footer">
									<a href="#" class="btn btn-primary btn-sm btn-history" key="<?php echo $row['id']; ?>" id="selesai">Detail Pesanan</a>
								</div>
							</div>

							<?php
									}
								}else{
							 ?>
							

							<div class="card" style="margin-top: 20px;">
								<div class="card-body" style="text-align: center;padding: 60px 20px;">
									<img src="<?php echo base_url().'assets/empty.svg' ?>" style="margin-bottom: 10px;">
									<p style="font-weight: 700;font-size: 14px;margin-bottom: 0px;">Belanja dan buat pesanan sekarang!</p>
									<p style="font-size: 14px;margin-bottom: 10px;">Dukung petani kita dan hasil pangan lainnya dengan memilih produk lokal dalam keseharianmu bersama kami!</p>
									<a href="" class="btn btn-success btn-sm">Belanja Sekarang</a>
								</div>
							</div>

						<?php } ?>
						</div>
						<div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
							

							<?php 
							$total_tr_batal = 0;
							$id_user = $this->session->userdata('id_user');
								$cek_batal_temp = $this->db->query("SELECT a.*, b.nama_bank, b.no_rekening, c.keterangan FROM transaksi_temp a 
									LEFT JOIN data_bank b ON a.id_bank_pembayaran = b.id 
									LEFT JOIN pembatalan c ON a.id_batal = c.id
									WHERE a.id_user='".$id_user."' AND a.status='0' ");

								$total_tr_batal += $cek_batal_temp->num_rows();

									$cek_batal_temp = $cek_batal_temp->result_array();

									foreach ($cek_batal_temp as $row) {

										@$pembayaran_batal = @$row['grand_total'] + @$row['ongkir'];
							?>

							<div class="card" style="margin-top: 20px;">
								<div class="card-body">
									<div class="row">
										<div class="col-md-3">
											<p class="text-comp is-grey mb5">Nomor Pesanan</p>
											<p style="font-size: 14px;font-weight: 700;margin-bottom: 5px;"><?php echo $row['kode_transaksi']; ?></p>
											<p class="text-comp is-grey">20 april 2020 20:31</p>
										</div>
										<div class="col-md-3">
											<p class="text-comp is-grey mb5">Status Pesanan</p>
											<span style="padding: 5px;border-radius: 5px;background-color:#f43f3f;color:#FFF;" class="text-comp">Batal</span>
											<p class="text-comp is-grey" style="margin-top:5px;margin-bottom: 5px;"><b>Keterangan: <?php echo $row['keterangan']; ?></b></p>
										</div>
										
										<div class="col-md-3">
											<p class="text-comp is-grey mb5">Status Pesanan</p>
											<p style="font-size: 14px;font-weight: 700;margin-bottom: 5px;">BANK <?php echo $row['nama_bank']; ?></p>
											<p class="text-comp is-grey"><?php echo $row['no_rekening']; ?></p>
										</div>
											<div class="col-md-3">
											<p class="text-comp is-grey mb5">Total Pembayaran</p>
											<p style="font-size: 14px;font-weight: 700;margin-bottom: 5px;"><?php echo rupiah(@$pembayaran_batal); ?></p>
										</div>
									</div>
								</div>
								<div class="card-footer">
									<a href="#" class="btn btn-primary btn-sm btn-history" key="<?php echo $row['id']; ?>" id="batal">Detail Pesanan</a>
								</div>
							</div>

							<?php
									}
								
							 ?>


							<?php 
							$id_user = $this->session->userdata('id_user');
								$cek_batal = $this->db->query("SELECT a.*, b.nama_bank, b.no_rekening, c.keterangan FROM transaksi a 
									LEFT JOIN data_bank b ON a.id_bank_pembayaran = b.id
									LEFT JOIN pembatalan c ON a.id_batal = c.id
									 WHERE a.id_user='".$id_user."' AND a.status='0' ");
								$total_tr_batal += $cek_batal->num_rows();
									$cek_batal = $cek_batal->result_array();

									foreach ($cek_batal as $row) {

										$pembayaran_batal = $row['grand_total'] + $row['ongkir'];
							?>

							<div class="card" style="margin-top: 20px;">
								<div class="card-body">
									<div class="row">
										<div class="col-md-3">
											<p class="text-comp is-grey mb5">Nomor Pesanan</p>
											<p style="font-size: 14px;font-weight: 700;margin-bottom: 5px;"><?php echo $row['kode_transaksi']; ?></p>
											<p class="text-comp is-grey">20 april 2020 20:31</p>
										</div>
										<div class="col-md-3">
											<p class="text-comp is-grey mb5">Status Pesanan</p>
											<span style="padding: 5px;border-radius: 5px;background-color:#f43f3f;color:#FFF;" class="text-comp">Batal</span>
											<p class="text-comp is-grey" style="margin-top:5px;margin-bottom: 5px;"><b>Keterangan: <?php echo $row['keterangan']; ?></b></p>
										</div>
										<div class="col-md-3">
											<p class="text-comp is-grey mb5">Status Pesanan</p>
											<p style="font-size: 14px;font-weight: 700;margin-bottom: 5px;">BANK <?php echo $row['nama_bank']; ?></p>
											<p class="text-comp is-grey"><?php echo $row['no_rekening']; ?></p>
										</div>
											<div class="col-md-3">
											<p class="text-comp is-grey mb5">Total Pembayaran</p>
											<p style="font-size: 14px;font-weight: 700;margin-bottom: 5px;"><?php echo rupiah($pembayaran_batal); ?></p>
										</div>
									</div>
								</div>
								<div class="card-footer">
									<a href="#" class="btn btn-primary btn-sm btn-history" key="<?php echo $row['id']; ?>" id="batal">Detail Pesanan</a>
								</div>
							</div>

							<?php
									}
								
							 ?>
							
							<?php if($total_tr_batal == 0){ ?>

							<div class="card" style="margin-top: 20px;">
								<div class="card-body" style="text-align: center;padding: 60px 20px;">
									<img src="<?php echo base_url().'assets/empty.svg' ?>" style="margin-bottom: 10px;">
									<p style="font-weight: 700;font-size: 14px;margin-bottom: 0px;">Belanja dan buat pesanan sekarang!</p>
									<p style="font-size: 14px;margin-bottom: 10px;">Dukung petani kita dan hasil pangan lainnya dengan memilih produk lokal dalam keseharianmu bersama kami!</p>
									<a href="" class="btn btn-success btn-sm">Belanja Sekarang</a>
								</div>
							</div>

						<?php } ?>

						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>




<div class="modal fade" id="historymodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel" style="font-size: 14px;font-weight: 700;">Detail Pesanan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
<div class="loading" style="display: block;">
							    <div class="loading-animation">
							      <svg><path d="M442 79.1H0V65.5h412.4v-7.1H0V0h442v79.1zm0 7.1V107H181.2v-7.1H0V86.2h442zM50.1 24.6v7.2h53.3v-7.2H50.1zm0-16.8v7.1h89.3V7.8H50.1zM19.3 38.9c10.6 0 19.2-8.7 19.2-19.4C38.5 8.7 30 0 19.3 0A19.4 19.4 0 0 0 0 19.5c0 10.7 8.6 19.4 19.3 19.4z"/></svg>
							    </div>
							  </div>

							  <div class="load_detail_belanja"></div>
      </div>
   
    </div>
  </div>
</div>



    <div class="modal fade" id="modalbatal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Alasan Pembatalan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
          <?php $batal = $this->db->get_where('pembatalan', array('flag' => '1'))->result_array();
            foreach ($batal as $row) { ?>
               

            <label class="radionya"><?php echo $row['keterangan']; ?>
              <input  type="radio" name="batal" class="batalnya" value="<?php echo $row['id']; ?>" >
              <span class="checkmark"></span>
            </label>
             <?php
            }
           ?>

           <div class="alert alert-danger alert-batal" role="alert" style="display: none;">
			  Keterangan harus diisi
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary btn-send-batal">Kirim</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	$('.btn-history').click(function(){
		var id_transaksi = $(this).attr('key');
		var status = $(this).attr('id');

		$('#historymodal').modal('show');

		$.ajax({
				url : "<?php echo base_url().'web/load_detail_history' ?>",
				type: "POST",
				data : {id_transaksi:id_transaksi, status:status}
			}).done(function(response){
				
				$('.load_detail_belanja').html(response);
				$('.loading').css('display', 'none');
				
			});

	});

	$('.btn-send-batal').click(function(){

		var id_transaksi = $(this).attr('key');
		var status = $(this).attr('status');
		var id_batal = $(".batalnya[name=batal]:checked").val();
		
		if($(".batalnya[name=batal]:checked").val()){
			$('.alert-batal').hide();
			
		}else{
			$('.alert-batal').show();
			return false;
		}
			$.ajax({
				url : "<?php echo base_url().'web/batal_pesanan_temp' ?>",
				type: "POST",
				data : {id_transaksi:id_transaksi, status:status, id_batal:id_batal}
			}).done(function(response){
				
				location.reload();
				
			});
	});


	function batal_pesanan(id_transaksi,status){
		$('.btn-send-batal').attr('key', id_transaksi);
		$('.btn-send-batal').attr('status', status);
		$('#historymodal').modal('hide');
		$('#modalbatal').modal('show');
	}
</script>