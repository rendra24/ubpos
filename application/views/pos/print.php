<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>receipt</title>
  <!-- <link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@400&display=swap" rel="stylesheet"> -->
  <link href="https://fonts.googleapis.com/css2?family=Nanum+Gothic+Coding&family=Roboto+Mono:wght@500&display=swap" rel="stylesheet">
  <style>
    @page { margin: 0 }
body { margin: 0 }
.sheet {
  margin: 0;
  overflow: hidden;
  position: relative;
  box-sizing: border-box;
  page-break-after: always;
}

/** Paper sizes **/
body.A3               .sheet { width: 297mm; height: 419mm }
body.A3.landscape     .sheet { width: 420mm; height: 296mm }
body.A4               .sheet { width: 210mm; height: 296mm }
body.A4.landscape     .sheet { width: 297mm; height: 209mm }
body.A5               .sheet { width: 148mm; height: 209mm }
body.A5.landscape     .sheet { width: 210mm; height: 147mm }
body.letter           .sheet { width: 216mm; height: 279mm }
body.letter.landscape .sheet { width: 280mm; height: 215mm }
body.legal            .sheet { width: 216mm; height: 356mm }
body.legal.landscape  .sheet { width: 357mm; height: 215mm }

/** Padding area **/
.sheet.padding-10mm { padding: 5mm;padding-left: 0mm;margin-bottom: 55mm; }

/** For screen preview **/
@media screen {
  body { background: #e0e0e0 }
  .sheet {
    background: white;
    box-shadow: 0 .5mm 2mm rgba(0,0,0,.3);
    margin: 5mm auto;
  }
}

/** Fix for Chrome issue #273306 **/
@media print {
           body.A3.landscape { width: 420mm }
  body.A3, body.A4.landscape { width: 297mm }
  body.A4, body.A5.landscape { width: 210mm }
  body.A5                    { width: 148mm }
  body.letter, body.legal    { width: 216mm }
  body.letter.landscape      { width: 280mm }
  body.legal.landscape       { width: 357mm }
}

@font-face {
   font-family: 'Nanum Gothic Coding', monospace;
}



* {
   font-family: 'Nanum Gothic Coding', monospace;
   font-size: 12px;
   font-weight: normal;
    word-spacing: 0px;
    letter-spacing: -1px;
    line-height: 1.2em;
}

body.receipt .sheet { width: 58mm;height: auto; } /* change height as you like */
@media print {
 body.receipt { width: 58mm } 
 .page-break { display: block; page-break-before: always; }
 * {
   font-family: 'Nanum Gothic Coding', monospace;
   font-size: 12px;
    word-spacing: 0px;
    word-break: break-all;
    letter-spacing: 1px;
    line-height: 1.2em;
}
 } /* this line is needed for fixing Chrome's bug */




  </style>
</head>
<?php 
  $penjualan_detail = $this->db->query("SELECT c.nama , b.qty , b.total from penjualan a 
        LEFT JOIN penjualan_detail b ON a.id = b.id_penjualan
        LEFT JOIN produk c ON b.id_produk = c.id 
        WHERE a.id = ".$id_penjualan)->result_array();


        //penjualan
      $penjualan = $this->db->query("SELECT a.*,b.nama as kasir FROM penjualan a LEFT JOIN admin b ON a.uid = b.id WHERE a.id=".$id_penjualan)->row_array();
      $harga_subtotal = decimals($penjualan['sub_total']);
      $total_bayar = decimals($penjualan['bayar']);
      $total_kembalian = decimals($penjualan['kembali']);

      $text_total = strlen($harga_subtotal);
      $text_bayar = strlen($total_bayar);
      $text_kembali = strlen($total_kembalian);
      $kasir = strlen($penjualan['kasir']);


       if(strlen($penjualan['kasir']) >= 13){
          $nama_kasir = substr($penjualan['kasir'], 0,13);
        }else{
          $nama_kasir = $penjualan['kasir'];
        }
       


    

  
 ?>
<body class="receipt" onload="window.print()">
  <section class="sheet padding-10mm">
    <center>UPOS <br> Kitaura-75-1 Yonezucho, Nishio, Aichi 445-0802, Jepang <br>+81 70-5507-5207<br><br></center>
    <div style="width: 100%;">
      <div style="width: 40%;float: left;">Cashir Staf</div>
      <div style="width: 60%;float: right;text-align: right;"><?php echo @$nama_kasir; ?></div>
    </div>
    <table style="width: 100%;">
      <tr>
        <td colspan="2"><div style=" border-bottom: 1px dotted;margin-top: 5px;margin-bottom: 5px;"></div></td>
      </tr>
      <?php 
      foreach ($penjualan_detail as $row) {


        $jumlah = $row['qty'].' '; //2 text
        $nama = $row['nama']; //7 text
        $harga = decimals($row['total']); // 7 text

        if(strlen($row['nama']) > 18){
          $nama = substr($row['nama'], 0,18);
        }

        $total_text =  strlen($jumlah) + strlen($nama) + strlen($harga);
        $total_spaci = ((int)32 - (int)$total_text);

        $spaci_array= array();
        for ($i=1; $i <= $total_spaci; $i++) { 
          $spaci_array[] =  ' ';

        } 

        $imp = implode("|",$spaci_array);
        $spaci = str_replace("|", "", $imp);
?>


      <tr>
        <td><?php echo $jumlah.$nama; ?></td>
        <td style="text-align: right;"><?php echo $harga ?></td>
      </tr>

      <?php

    }
       ?>

       <tr>
        <td colspan="2"><div style=" border-bottom: 1px dotted;margin-top: 5px;margin-bottom: 5px;"></div></td>
      </tr>
      <tr>
        <td>Total Bill</td>
        <td style="text-align: right;"><?php echo $harga_subtotal; ?></td>
      </tr>
       <tr>
        <td colspan="2"><div style=" border-bottom: 1px dotted;margin-top: 5px;margin-bottom: 5px;"></div></td>
      </tr>
      <tr>
        <td>Nominal Payment</td>
        <td style="text-align: right;"><?php echo $total_bayar; ?></td>
      </tr>
      <tr>
        <td>Change Money</td>
        <td style="text-align: right;"><?php echo $total_kembalian; ?></td>
      </tr>
    </table>
    
  </section>
  <script type="text/javascript">
     setTimeout(function () { window.print(); }, 500);
        window.onfocus = function () { setTimeout(function () { window.close(); }, 500); }
  </script>
</body>
</html>