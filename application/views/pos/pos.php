<title>UPOS</title>
<link rel="shortcut icon" href="<?php echo base_url().'favicon_old.ico'; ?>" />
<link rel="stylesheet" href="<?php echo base_url().'admin_assets/css/style.css'; ?>">
<link rel="stylesheet" href="<?php echo base_url().'admin_assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css'; ?>">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<!-- 
 <link rel="stylesheet" href="https://twitter.github.io/typeahead.js/css/examples.css" /> -->
 <style type="text/css">
   .tt-menu {
    width: 422px;
    margin: 12px 0;
    padding: 8px 0;
    background-color: #fff;
    border: 1px solid #ccc;
    border: 1px solid rgba(0, 0, 0, 0.2);
    -webkit-border-radius: 8px;
    -moz-border-radius: 8px;
    border-radius: 8px;
    -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
    -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
    box-shadow: 0 5px 10px rgba(0,0,0,.2);
  }
  .tt-menu, .gist {
    text-align: left;
  }
  .tt-suggestion {
    padding: 3px 20px;
    line-height: 24px;
  }

  .tt-suggestion:hover {
    cursor: pointer;
    color: #fff;
    background-color: #0097cf;
  }

  .tt-suggestion.tt-cursor {
    color: #fff;
    background-color: #0097cf;

  }

  .twitter-typeahead{
    width: 100%;
  }

  .form-control{
    border-color: #000;
  }

</style>



<script src="https://twitter.github.io/typeahead.js/js/handlebars.js"></script>
<script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<nav class="navbar navbar-light bg-light" style="background: linear-gradient(120deg, #00e4d0, #5983e8);">
  <a class="navbar-brand" href="#" style="color: #FFF;margin: 0 auto;">UPOS</a>
</nav>

<div>
  <div>

    <div class="row">

      <div class="col-lg-8 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">





            <form id="formstok">



              <table class="table">
                <tr>
                  <th>Barcode</th>
                  <th>Produk</th>
                  <th>Harga</th>
                  <th>Total</th>
                  <th></th>
                </tr>
                <tr style="background-color: #f2f8f9;">
                  <td>
                    <input type="text" name="cari" class="textcari form-control" placeholder="Barcode , Kode">
                  </td>
                  <td>

                   <div id="prefetch">
                    <input type="text" id="search_box" name="nama_produk" class="form-control nama_produk typeahead" placeholder="Produk">  
                  </div>          
                  <input type="hidden" name="id_produk" class="id_produk">
                  <input type="hidden" name="id_customer" class="id_customer">
                </td>
                <td>
                  <p class="hargaproduk1"></p>
                  <p class="hargaproduk3" style="display: none;"></p>
                  <p class="hargaproduk6" style="display: none;"></p>
                  <p class="hargaproduk12" style="display: none;"></p>
                  <p class="hargaproduk24" style="display: none;"></p>
                  <input type="hidden" name="harga" class="harga">
                </td>
                <td>

                  <input type="number" class="form-control qty" placeholder="Jumlah">

                </td>
                <td>
                  <button type="button" class="btn btn-success btn-add">Tambah</button>
                </td>
              </tr>
            </table>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th tyle="width: 200px;"> Nama Produk </th>
                  <th style="text-align: center;"> Total </th>
                  <th style="text-align: right;"> Harga </th>
                  <th style="text-align: right;"> Total Harga </th>
                  <th></th>
                </tr>
              </thead>
              <tbody id="disini">
                <?php 
                $get = $this->db->query('SELECT b.*, c.nama from temp_penjualan a 
                  LEFT JOIN temp_penjualan_detail b ON a.id_temp = b.id_temp_penjualan 
                  LEFT JOIN produk c ON b.id_produk =  c.id
                  WHERE a.id_user='.$_SESSION['userid'])->result_array();

                foreach ($get as $row) {

                 ?>
                 <tr>
                   <td style="width: 200px;">
                    <?php echo $row['nama']; ?>
                  </td>
                  <td style="text-align: center;">

                   <!--  <input type="number" class="form-control"  name="jumlah[]" id="jumlah1" onkeyup="hitung(1);" value="<?php //echo $row['qty']; ?>"> -->

                   <?php echo $row['qty']; ?>x

                 </td>
                 <td style="text-align: right;">

                  <?php echo $row['harga']; ?>

                </td>
                <td style="text-align: right;">
                 <p id="total1" style="margin: 0px;"><?php echo $row['total']; ?></p>
                 <input type="hidden" name="status[]" value="in">

               </td>
               <td>
                <button type="button" class="btn btn-danger btn-sm btn-delete" key="<?php echo $row['id']; ?>"><span class="mdi mdi-delete"></span></button>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>

    </form>
  </div>
</div>
</div>

<div class="col-lg-4" style="padding-top:10px;">
  <div class="card">
    <div class="card-body">

      <div style="width: 100%;box-shadow: 0 0.5em 1em -0.125em rgba(10,10,10,.1), 0 0 0 1px rgba(10,10,10,.02);border-radius: 8px;
      border: 1px solid rgba(0,0,0,.125);padding: 15px;margin-bottom: 15px;">
      <div class="row" style="margin-bottom: 10px;">
        <div class="col-md-7">
          <p style="margin: 0px;">Tambah Pelanggan</p>
        </div>
        <div class="col-md-5" style="text-align: right;">
          <button type="button" class="btn btn-success btn-add-new" data-toggle="modal" data-target="#customerMdl">Tambah Baru</button>
        </div>
      </div>
      
      <button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#exampleModal">Pilih Pelanggan</button>

      <div style="display: none;" class="frame-customer">
        <table class="table" style="font-size: 12px;">
          <tr>
            <td>Nama Pelanggan</td>
            <td class="customer_name" style="text-align: right;"></td>
          </tr>
          <tr>
            <td>Telephone</td>
            <td class="customer_phone" style="text-align: right;"></td>
          </tr>
          <tr>
            <td>Email</td>
            <td class="customer_email" style="text-align: right;"></td>
          </tr>
        </table>
      </div>

      <div class="row" style="margin-top: 15px;">
        <div class="col-lg-6"><p style="margin-top: 10px;">Invoice</p></div>
        <div class="col-lg-6">
          <div class="form-group">
            <select class="form-control invoice-type">
              <option value="1">English</option>
              <option value="2">Indonesian</option>
            </select>
          </div>
        </div>
      </div>
    </div>


    <?php 
    $detail = $this->db->get_where('temp_penjualan', array('id_user' => $this->session->userdata('userid')))->row_array();
    ?>

    <table style="width: 100%;">
      <tr>
        <td><p>Sub Total</p></td>
        <td style="text-align: right;"><p style="font-size: 22px;" class="subtotal"><?php echo decimals($detail['total']); ?></p></td>
      </tr>
       <!--  <tr>
          <td><p>Ppn(10%)</p></td>
          <td style="text-align: right;"><p style="font-size: 22px;">0</p></td>
        </tr>
         <tr>
         -->          
         <td><p>Ongkir</p></td>
         <td style="text-align: right;"><input type="number" name="ongkir" class="form-control" style="text-align: right;color: #000;font-size: 16px;" placeholder="Postage" id="ongkir" min="0">
         </td>
       </tr>
       </tr>      
         <td><p>COD</p></td>
         <td style="text-align: right;"><input type="number" name="cod" class="form-control" style="text-align: right;color: #000;font-size: 16px;" placeholder="COD Fee" id="cod" min="0">
         </td>
       </tr>
       <tr>
        <td><p>Grand Total</p></td>
        <td style="text-align: right;"><p style="font-size: 22px;" class="total" key="<?php echo $detail['total']; ?>"><?php echo decimals($detail['total']); ?></p></td>
      </tr>
      <tr>
        <td colspan="2">
          <select class="form-control pembayaran" style="width: 100%;text-align: right;color: #000;font-size: 16px;margin-bottom: 15px;">
            <?php $get_bayar = $this->db->get_where('pembayaran', array('status' => 1))->result_array();
            foreach ($get_bayar as $row) {
              ?>
              <option value="<?php echo $row['id_pembayaran']; ?>"><?php echo $row['nama_pembayaran']; ?></option>
              <?php
            }
            ?>

          </select>
        </td>
      </tr>
      <tr>
        <td colspan="2"><input type="text" name="bayar" class="form-control bayar" style="width: 100%;text-align: right;color: #000;font-size: 16px;" placeholder="Nominal payment">
          <div class="invalid-feedback">
            Total Pembayaran harus di isi.
          </div></td>
        </tr>
        <tr>
          <td><p>Change Money</p></td>
          <td style="text-align: right;"><p style="font-size: 22px;" class="kembalian">0</p></td>
        </tr>
        <tr>
          <td colspan="2"><button type="button" class="btn btn-success btn-block btn-bayar" style="height: 45px;">BAYAR</button></td>
        </tr>
      </table>

    </div>
  </div>
</div>

</div>
</div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pelanggan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <div id="customer">
        <input type="text"  class="form-control typeahead" placeholder="Customer Name">  
      </div> 
    </div>
  </div>
</div>
</div>

<div class="modal fade" id="customerMdl" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form class="form-customer">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Pelanggan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
         <div class="form-group">
          <label for="exampleInputEmail1">Nama <span class="text-danger" title="Reuired">*</span></label>
          <input type="text" class="form-control form-customer" name="nama" required>
        </div>

        <div class="form-group">
          <label for="exampleInputEmail1">Email <span class="text-danger" title="Reuired">*</span></label>
          <input type="text" class="form-control form-customer" name="email" required>
        </div>


        <div class="form-group">
          <label for="exampleInputEmail1">Telephone <span class="text-danger" title="Reuired">*</span></label>
          <input type="number" class="form-control form-customer"  name="tlp" required>  
        </div>

        <div class="form-group">
          <label for="exampleInputEmail1">Kota <span class="text-danger" title="Reuired">*</span></label>
          <input type="text" class="form-control form-customer" name="kota" required>
        </div>

        <div class="form-group">
          <label>Alamat <span class="text-danger" title="Reuired">*</span></label>
          <textarea class="form-control form-customer" name="alamat" id="alamat" required></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-success">Simpan Pelanggan</button>
      </div>
    </form>
  </div>
</div>
</div>


<script type="text/javascript">

  function decimals(angka, prefix){
    var number_string = angka.toString().replace(/[^,\d]/g, ''),
    split       = number_string.split(','),
    sisa        = split[0].length % 3,
    rupiah        = split[0].substr(0, sisa),
    ribuan        = split[0].substr(sisa).match(/\d{3}/gi);

      // tambahkan titik jika yang di input sudah menjadi angka ribuan
      if(ribuan){
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
      }

      rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
      return prefix == undefined ? rupiah : (rupiah ? + rupiah : '');
    }


    function get_total(){
      $.ajax({
        type: "POST",
        url: "<?php echo base_url().'kasir/get_total'; ?>",
        dataType: 'json',
        success: function(data) {

          var ongkir = $('#ongkir').val();

          if(ongkir == ''){
            ongkir = 0
          }

           var cod = $('#cod').val();

          if(cod == ''){
            cod = 0
          }


          var grand_total = parseInt(data.total) + parseInt(ongkir) + parseInt(cod);

          $('.subtotal').text(data.total_text);
          $('.total').text(decimals(grand_total));
          $('.total').attr('key',data.total);
        }
      });

      $('.btn-delete').click(function(){
        var id = $(this).attr('key');
        $.ajax({
          type: "POST",
          url: "<?php echo base_url().'kasir/delete_produk'; ?>",
          data: {id:id},
          dataType: 'html',
          success: function(data) {
           $('#disini').html(data);

           get_total();
         }
       });
      });

    }


    $(document).ready(function(){
      $('.textcari').focus();

      $('.btn-delete').click(function(){
        var id = $(this).attr('key');
        $.ajax({
          type: "POST",
          url: "<?php echo base_url().'kasir/delete_produk'; ?>",
          data: {id:id},
          dataType: 'html',
          success: function(data) {
           $('#disini').html(data);

           get_total();
         }
       });
      });


      var sample_data = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch:'<?php echo base_url(); ?>kasir/fetch',
        remote:{
          url:'<?php echo base_url(); ?>kasir/fetch/%QUERY',
          wildcard:'%QUERY'
        }
      });

      $('#prefetch .typeahead').typeahead(null, {
        nama: 'sample_data',
        display: 'nama',
        source:sample_data,
        limit:10,
        templates:{
          suggestion:Handlebars.compile('<div class="row" style="margin-left:0px !important;margin-right:0px !important;"><div class="col-md-12">{{nama}}</div></div>')
        }
      });


      var customer_data = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch:'<?php echo base_url(); ?>kasir/fetch_customer',
        remote:{
          url:'<?php echo base_url(); ?>kasir/fetch_customer/%QUERY',
          wildcard:'%QUERY'
        }
      });

      $('#customer .typeahead').typeahead(null, {
        nama: 'customer_data',
        display: 'nama',
        source:customer_data,
        limit:10,
        templates:{
          suggestion:Handlebars.compile('<div class="row" style="margin-left:0px !important;margin-right:0px !important;"><div class="col-md-12">{{nama}}</div></div>')
        }
      });

      $('#customer').on('typeahead:selected', function (e, data) {

        if(data.type == 'customer'){
         var code = data.id; 
         $('.id_customer').val(code);

         $.ajax({
          type: "POST",
          url: "<?php echo base_url().'kasir/cari_customer_by_id'; ?>",
          data: {code:code},
          dataType: 'json',
          success: function(data) {
            $('.customer_name').text(data.nama);
            $('.customer_phone').text(data.tlp);
            $('.customer_email').text(data.email);
            $('.frame-customer').show();
            $('#exampleModal').modal('hide');
          }
        });
       }   

     });


      $(".form-customer").submit(function(event){
        event.preventDefault();


        $.ajax({
          type: "POST",
          url: "<?php echo base_url().'kasir/simpan_pelanggan'; ?>",
          data: $(this).serialize(),
          success: function(data) {

            if(data == 1)
            {
              swal({
                icon: "success",
                text: "Success add data customer",
              });

              $('#customerMdl').modal('hide');
              $('.form-customer').val('');
            }
          }
        });

      });

      $('.typeahead').on('typeahead:selected', function (e, data) {
        if(data.type == 'produk'){
          var code = data.id;
          $('.id_produk').val(code);

          $.ajax({
            type: "POST",
            url: "<?php echo base_url().'kasir/cari_produk_by_id'; ?>",
            data: {code:code},
            dataType: 'json',
            success: function(data) {
              $('.textcari').val(data.kode);
              $('.hargaproduk1').text(data.harga);
              $('.hargaproduk3').text(data.harga_3_satuan);
              $('.hargaproduk6').text(data.harga_6_satuan);
              $('.hargaproduk12').text(data.harga_12_satuan);
              $('.hargaproduk24').text(data.harga_24_satuan);

              $('.hargaproduk1').attr('key', data.harga);
              $('.hargaproduk3').attr('key', data.harga_3_satuan);
              $('.hargaproduk6').attr('key', data.harga_6_satuan);
              $('.hargaproduk12').attr('key', data.harga_12_satuan);
              $('.hargaproduk24').attr('key', data.harga_24_satuan);
              $('.harga').val(data.harga);
              $('.qty').focus();
            }
          });
        }
      });


      $('.btn-add').click(function(){
        var id_produk = $('.id_produk').val();
        var harga_produk = $('.harga').val();
        var qty = $('.qty').val();

        $.ajax({
          type: "POST",
          url: "<?php echo base_url().'kasir/add_temp'; ?>",
          data: {id_produk:id_produk, qty:qty, harga_produk:harga_produk},
          dataType: 'html',
          success: function(data) {
            $('#disini').html(data);

            get_total();

            $('.textcari').val('');
            $('.nama_produk').val('');
            $('.qty').val('');
            $('.hargaproduk1').hide();
            $('.hargaproduk3').hide();
            $('.hargaproduk6').hide();
            $('.hargaproduk12').hide();
            $('.hargaproduk24').hide();
            $('.textcari').focus();

          }
        });

      });

    });



$('.textcari').keyup(function(event){
  if (event.keyCode === 13) {
    var code = $(this).val();
    if(code != ''){
      $.ajax({
        type: "POST",
        url: "<?php echo base_url().'kasir/cari_produk'; ?>",
        data: {code:code},
        dataType: 'json',
        success: function(data) {
          $('.nama_produk').val(data.nama);
          $('.id_produk').val(data.id);
          $('.hargaproduk1').text(data.harga);
          $('.hargaproduk3').text(data.harga_3_satuan);
          $('.hargaproduk6').text(data.harga_6_satuan);
          $('.hargaproduk12').text(data.harga_12_satuan);
          $('.hargaproduk24').text(data.harga_24_satuan);

          $('.hargaproduk1').attr('key', data.harga);
          $('.hargaproduk3').attr('key', data.harga_3_satuan);
          $('.hargaproduk6').attr('key', data.harga_6_satuan);
          $('.hargaproduk12').attr('key', data.harga_12_satuan);
          $('.hargaproduk24').attr('key', data.harga_24_satuan);
          $('.harga').val(data.harga);
          $('.qty').focus();
        }
      });

    }else{
      $('.nama_produk').focus();
    }
  }
});


$(".nama_produk").keyup(function(event) {
  if (event.keyCode === 13) {
   $('.qty').focus();
 }
});

$(".qty").keyup(function(event) {
  var qty = $('.qty').val();

  if(qty  < 3){
    var harga = $('.hargaproduk1').attr('key');
    $('.harga').val(harga);

    $('.hargaproduk1').show();
    $('.hargaproduk3').hide();
    $('.hargaproduk6').hide();
    $('.hargaproduk12').hide();
    $('.hargaproduk24').hide();
  }else if(qty >=3 && qty < 6){

   var harga = $('.hargaproduk3').attr('key');
   $('.harga').val(harga);

   $('.hargaproduk3').show();
   $('.hargaproduk1').hide();
   $('.hargaproduk6').hide();
   $('.hargaproduk12').hide();
   $('.hargaproduk24').hide();
 }else if(qty >= 6 && qty < 12){
   var harga = $('.hargaproduk6').attr('key');
   $('.harga').val(harga);

   $('.hargaproduk6').show();
   $('.hargaproduk1').hide();
   $('.hargaproduk3').hide();
   $('.hargaproduk12').hide();
   $('.hargaproduk24').hide();
 }else if(qty >= 12 && qty < 24){
   var harga = $('.hargaproduk12').attr('key');
   $('.harga').val(harga);

   $('.hargaproduk12').show();
   $('.hargaproduk1').hide();
   $('.hargaproduk3').hide();
   $('.hargaproduk6').hide();
   $('.hargaproduk24').hide();
 }else if(qty >= 24){

   var harga = $('.hargaproduk24').attr('key');
   $('.harga').val(harga);

   $('.hargaproduk24').show();
   $('.hargaproduk1').hide();
   $('.hargaproduk3').hide();
   $('.hargaproduk6').hide();
   $('.hargaproduk12').hide();
 }else{
  $('.hargaproduk1').hide();
  $('.hargaproduk3').hide();
  $('.hargaproduk6').hide();
  $('.hargaproduk12').hide();
  $('.hargaproduk24').hide();
}

if (event.keyCode === 13) {

  if(qty == ''){
    $('.bayar').focus();
    return false;
  }
  var id_produk = $('.id_produk').val();
  var harga_produk = $('.harga').val();

  $.ajax({
    type: "POST",
    url: "<?php echo base_url().'kasir/add_temp'; ?>",
    data: {id_produk:id_produk, qty:qty, harga_produk:harga_produk},
    dataType: 'html',
    async:true,
    success: function(data) {
      $('#disini').html(data);

      get_total();

      $('.textcari').val('');
      $('.nama_produk').val('');
      $('.qty').val('');
      $('.hargaproduk1').hide();
      $('.hargaproduk3').hide();
      $('.hargaproduk6').hide();
      $('.hargaproduk12').hide();
      $('.hargaproduk24').hide();
      $('.textcari').focus();

    }
  });
}
});



function get_harga(id,value){
  $.ajax({
    type: "POST",
    url: "<?php echo base_url().'inventori/detail_produk'; ?>",
    data: {value:value},
    dataType: 'json',
    success: function(data) {
      $('#harga'+id).val(data.harga);
      $('#satuan'+id).text(data.satuan);
    }
  });


}

function hitung(id)
{
  var jml = $('#jumlah'+id).val();
  var harga = $('#harga'+id).val();
  var hasil = jml * harga;

  $('#total'+id).val(hasil);

}

$('#ongkir').on("keyup", function(event){
 $(this).val($(this).val().replace(/[^\d].+/, ""));
 if ((event.which < 48 || event.which > 57)) {
  event.preventDefault();

}
var ongkir = $('#ongkir').val();
if(ongkir == ''){
  ongkir = 0
}

var total = parseInt($('.total').attr('key')) + parseInt(ongkir);
var hasil = parseInt($('.bayar').val()) - parseInt(total);

if(isNaN(parseFloat(hasil)) == true){
  hasil = 0
}

$('.kembalian').text(hasil);
$('.total').text(total);

});


$('#cod').on("keyup", function(event){

     var cod = $('#cod').val();

          if(cod == ''){
            cod = 0
          }

    var total = parseInt($('.total').attr('key')) + parseInt(cod);
var hasil = parseInt($('.bayar').val()) - parseInt(total);

if(isNaN(parseFloat(hasil)) == true){
  hasil = 0
}

$('.kembalian').text(hasil);
$('.total').text(total);


});


$(".bayar").on("keyup",function (event) {    
 $(this).val($(this).val().replace(/[^\d].+/, ""));
 if ((event.which < 48 || event.which > 57)) {
  event.preventDefault();

}
var ongkir = $('#ongkir').val();
if(ongkir == ''){
  ongkir = 0
}

  var cod = $('#cod').val();

          if(cod == ''){
            cod = 0
          }

var total = parseInt($('.total').attr('key')) + parseInt(ongkir) + parseInt(cod);
var hasil = parseInt($(this).val()) - parseInt(total);

$('.kembalian').text(hasil);

});

$('.bayar').keyup(function(event){
  if(event.keyCode === 13){
   if($('.bayar').val() == ''){
    $('.bayar').addClass('is-invalid');
    return false;
  }else{
    $('.bayar').removeClass('is-invalid');
  }

  
  $('.swal-button--confirm').focus();
  swal({
    title: "Are you sure?",
    text: "Will complete the transaction",
    icon: "warning",

    buttons: {
      cancel: true,
      confirm: "Done",
    },
  })
  .then((willDelete) => {
    if (willDelete) {
      var bayar = $(this).val();
      var id_pembayaran = $('.pembayaran').val();
      var ongkir = $('#ongkir').val();
      var cod = $('#cod').val();
      var id_customer = $('.id_customer').val();
      var invoice = $('.invoice-type').val();

      $.ajax({
        type: "POST",
        url: "<?php echo base_url().'kasir/bayar'; ?>",
        data: {bayar:bayar, id_pembayaran:id_pembayaran, ongkir:ongkir, id_customer:id_customer, invoice:invoice, cod:cod},
        async:false,
        success: function(data) {
           location.reload();
          
          
          
        }
      });

    }
  });
}
});

$('.btn-bayar').click(function(){

  if($('.bayar').val() == ''){
    $('.bayar').addClass('is-invalid');
    return false;
  }else{
    $('.bayar').removeClass('is-invalid');
  }

  $('.swal-button--confirm').focus();
  swal({
    title: "Are you sure?",
    text: "Will complete the transaction",
    icon: "warning",

    buttons: {
      cancel: true,
      confirm: "Done",
    },
  })
  .then((willDelete) => {
    if (willDelete) {
      var bayar = $('.bayar').val();
      var id_pembayaran = $('.pembayaran').val();
      var ongkir = $('#ongkir').val();
      var id_customer = $('.id_customer').val();
      var invoice = $('.invoice-type').val();

      $.ajax({
        type: "POST",
        url: "<?php echo base_url().'kasir/bayar'; ?>",
        data: {bayar:bayar, id_pembayaran:id_pembayaran, ongkir:ongkir, id_customer:id_customer, invoice:invoice, cod:cod},
        async:false,
        success: function(data) {
          location.reload();
          // if(data.bahasa == 1){
          //   window.open('<?php echo base_url().'kasir/cetak/' ?>'+data.id_penjualan, '_blank');
          //   location.reload();
          // }

          //  if(data.bahasa == 0){
          //   window.open('<?php echo base_url().'kasir/cetak_indo/' ?>'+data.id_penjualan, '_blank');
          //   location.reload();
          // }
          
          
        }
      });

    }
  });
});


$('#formstok').submit(function(event){
 event.preventDefault();

 if($('#cabang').val() == '')
 {
  alert('cabang tidak boleh kosong');
  return false;
}


$.ajax({
  type: "POST",
  url: "<?php echo base_url().'inventori/simpan_stok_masuk'; ?>",
  data: $(this).serialize(),
  success: function(data) {
    window.location = '<?php echo base_url().'inventori/stok_masuk' ?>';
  }
});
});


</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>