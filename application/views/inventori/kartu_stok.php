<style type="text/css">
thead tr th:last-child
{
    text-align: center;
}
thead tr th
{
    text-align: center;
}
thead tr td
{
    text-align: center;
}
tbody tr td:last-child
{
    text-align: center;
}

</style>
<div class="main-panel">
  <div class="content-wrapper">


    <div class="row">

      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">

            <div class="row">
              <div class="col-lg-6">
                <h4 class="card-title">Kartu Stok</h4>
             </div>
             <div class="col-lg-6" style="text-align: right;">
              
          </div>
      </div>


      <div class="table-responsive">
        <table class="table table-hover datatables ">
            <thead>
                <tr>
                    <th>#</th>
                    <th width="11%;">Produk</th>
                    <th width="11%;">Kategori</th>
                    <th>Stok Masuk</th>  
                    <th>Stok Keluar</th>
                    <th>Stok Akhir</th>

                </tr>
            </thead>
            <tbody>
                <?php $no =1; foreach ($transaksi as $row) {

                    ?>
                    <tr>
                        <td scope="row"><?php echo $no; ?></td>
                        <td><?php echo $row['nama']; ?></td>
                        <td><?php echo $row['nama_kategori']; ?></td>
                        <td style="text-align: center;"><?php echo $row['masuk']; ?></td>
                        <td style="text-align: center;"><?php echo $row['keluar']; ?></td>
                        <td style="text-align: center;"><?php echo $row['masuk'] - $row['keluar']; ?></td>
                       

                    </tr>
                    <?php
                    $no++;
                } ?>
            </tbody>
        </table>
    </div>
</div>
</div>
</div>

</div>
</div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
     $('.datatables').DataTable();
  });
</script>

<script type="text/javascript">
    $('.hapus').click(function(event){
        event.preventDefault();
        var id_barang = $(this).attr('key');


        swal({
          title: "Apakah kamu yakin ?",
          text: "hapus data barang ini",
          icon: "warning",
          buttons: true,
          dangerMode: true,
      })
        .then((willDelete) => {
          if (willDelete) {

              $.ajax({
                type: "POST",
                url: "<?php echo base_url().'admin/remove_barang'; ?>",
                data: {id_barang:id_barang},
                success: function(data){

                    if(data == 1)
                    {
                     swal("Success!","Berhasil hapus barang.", "success")
                     .then((value) => {
                      location.reload();
                  });
                 }

             }
         });


          } else {

          }
      });




    });
</script>
