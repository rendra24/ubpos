<link rel="stylesheet" href="<?php echo base_url().'admin_assets/select2/select2.min.css'; ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'admin_assets/select2/select2-bootstrap.min.css' ?>">
<script src="<?php echo base_url().'admin_assets/select2/select2.full.js'; ?>"></script>

 <style type="text/css">
   .tt-menu {
    width: 422px;
    margin: 12px 0;
    padding: 8px 0;
    background-color: #fff;
    border: 1px solid #ccc;
    border: 1px solid rgba(0, 0, 0, 0.2);
    -webkit-border-radius: 8px;
    -moz-border-radius: 8px;
    border-radius: 8px;
    -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
    -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
    box-shadow: 0 5px 10px rgba(0,0,0,.2);
  }
  .tt-menu, .gist {
    text-align: left;
  }
  .tt-suggestion {
    padding: 3px 20px;
    line-height: 24px;
  }

  .tt-suggestion:hover {
    cursor: pointer;
    color: #fff;
    background-color: #0097cf;
  }

  .tt-suggestion.tt-cursor {
    color: #fff;
    background-color: #0097cf;

  }

  .twitter-typeahead{
    width: 100%;
  }


</style>



<script src="https://twitter.github.io/typeahead.js/js/handlebars.js"></script>
<script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>

<div class="main-panel">
  <div class="content-wrapper">

    <div class="row">

      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">





            <form id="formstok">
             <div class="row">
              <div class="col-lg-6">
               <h4 class="card-title">Product</h4><br>

               <!-- <div class="form-group">
                <label>Outlet</label>
                <select id="cabang" name="cabang" class="form-control">
                  <option value="">-- Pilih Outlet -- </option>
                  <?php 
                  //$userid = $_SESSION['userid'];
                  //$get = $this->db->get_where('cabang', array('id_user' => $userid))->result_array();

                  foreach ($get as $row) {
                    ?>
                    <option value="<?php //echo $row['id']; ?>"><?php //echo $row['nama']; ?></option>
                    <?php
                  }
                  ?>
                </select>
              </div> -->

            </div>
            <div class="col-lg-6" style="text-align: right;">
                <input type="hidden" name="status_tr" value="SM">
              <button type="submit" class="btn btn-success btn-fw btn-submit">Save</button>
            </div>
          </div>


         
            <table class="table table-striped">
              <thead>
                <tr>
                  <th> Barcode </th>
                  <th style="width: 25%;"> Name Product </th>
                  <th style="width: 15%;"> Total </th>
                  <th> Unit </th>
                  <th> Price </th>
                  <th> Total Price (Rp) </th>
                  <th></th>
                </tr>
              </thead>
              <tbody id="disini">
               <tr>
                <td>
                  <input type="text" name="barcode[]" class="form-control barcode" placeholder="Barcode" key="1">
                </td>
                 <td style="width: 200px;">          
                     <div class="prefetch">
                    <input type="text" id="search_box" name="nama_produk[]" class="form-control nama_produk typeahead" placeholder="Produk" key="1">  
                  </div> 
                  <input type="hidden" name="id_produk[]" class="id_produk1"> 
                </td>
                <td>
                

                  <input type="text" class="form-control"  name="jumlah[]" id="jumlah1" onkeyup="hitung(1);">
               
              </td>
              <td>
                <p id="satuan1"></p>
              </td>
              <td>
              

                <input type="text" class="form-control"  name="harga[]" id="harga1">
              
            </td>
            <td>
             

              <input type="text" class="form-control"  name="total[]" id="total1">
              <input type="hidden" name="status[]" value="in">
            
          </td>
          <td>

          </td>
        </tr>

      </tbody>
      <tfoot>
        <tr>
          <td colspan="6">
            <a id="tambah_produk" style="color: blue;cursor: pointer;"><i class="mdi mdi-plus"></i> Add Product</a>
          </td>
        </tr>
      </tfoot>
    </table>
 
</form>
</div>
</div>
</div>

</div>
</div>
</div>


<script type="text/javascript">


 

  function get_produk(){
    if (event.keyCode === 13) {
    var value = $(this).val();
    if(value != ''){
      $.ajax({
        type: "POST",
        url: "<?php echo base_url().'inventori/cari_produk'; ?>",
        data: {value:value},
        dataType: 'json',
        success: function(data) {
          $('#harga'+id).val(data.harga);
          $('#satuan'+id).text(data.satuan);
          $('.nama_produk'+id).val(data.nama);
        }
      });

    }else{
      $('.nama_produk').focus();
    }
  }
  }



  function get_harga(id,value){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url().'inventori/detail_produk'; ?>",
      data: {value:value},
      dataType: 'json',
      success: function(data) {
        $('#harga'+id).val(data.harga);
        $('#satuan'+id).text(data.satuan);

      }
    });


  }

  function hitung(id)
  {
    var jml = $('#jumlah'+id).val();
    var harga = $('#harga'+id).val();
    var hasil = jml * harga;

    $('#total'+id).val(hasil);

  }

  var sample_data = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch:'<?php echo base_url(); ?>kasir/fetch',
        remote:{
          url:'<?php echo base_url(); ?>kasir/fetch/%QUERY',
          wildcard:'%QUERY'
        }
      });


  $('#tambah_produk').click(function(){
    var jml= parseInt($('.nama_produk').last().attr('urut'))+1;

    var html = '<tr><td><input type="text" name="barcode" class="form-control barcode'+jml+'" placeholder="Barcode" key="'+jml+'"></td><td style="width: 200px;"><div class="prefetch'+jml+'">'+
    '<input type="text" id="search_box" name="nama_produk[]" class="form-control nama_produk'+jml+' typeahead'+jml+'"  placeholder="Produk"></div><input type="hidden" name="id_produk[]" class="id_produk'+jml+'"></td>'+
    '<td><input type="text" class="form-control"  name="jumlah[]" id="jumlah'+jml+'" onkeyup="hitung('+jml+');"></td>'+
    '<td><p id="satuan'+jml+'"></p></td>'+
    '<td><input type="text" class="form-control"  name="harga[]" id="harga'+jml+'"></td>'+
    '<td><input type="text" class="form-control"  name="total[]" id="total'+jml+'"><input type="hidden" name="status[]" value="in"></td>'+
    '<td></td></tr>';

    $('#disini').append(html);


  

      $('.prefetch'+jml+' .typeahead'+jml).typeahead(null, {
        nama: 'sample_data',
        display: 'nama',
        source:sample_data,
        limit:10,
        templates:{
          suggestion:Handlebars.compile('<div class="row" style="margin-left:0px !important;margin-right:0px !important;"><div class="col-md-12">{{nama}}</div></div>')
        }
      });



      $('.typeahead'+jml).on('typeahead:selected', function (e, data) {
        if(data.type == 'produk'){
          var code = data.id;
        
          $('.id_produk').val(code);

          $.ajax({
            type: "POST",
            url: "<?php echo base_url().'kasir/cari_produk_by_id'; ?>",
            data: {code:code},
            dataType: 'json',
            success: function(data) {
              $('.id_produk'+jml).val(data.id);
              $('#harga'+jml).val(data.harga);
              $('#satuan'+jml).text(data.satuan);
              $('.barcode'+jml).val(data.kode);
              $('#jumlah'+jml).focus();
            }
          });
        }
      });


         $('.barcode'+jml).keyup(function(event){
  if (event.keyCode === 13) {
    var id = $(this).attr('key');
    var value = $(this).val();
    if(id != ''){
      $.ajax({
        type: "POST",
        url: "<?php echo base_url().'inventori/cari_produk'; ?>",
        data: {value:value},
        dataType: 'json',
        success: function(data) {
          $('.id_produk'+jml).val(data.id);
          $('#harga'+jml).val(data.harga);
          $('#satuan'+jml).text(data.satuan);
          $('.nama_produk'+jml).val(data.nama);
          $('#jumlah'+jml).focus();
        }
      });

    }else{
      $('.nama_produk'+jml).focus();
    }
  }
});


  });




  $('#formstok').submit(function(event){
   event.preventDefault();

   if($('#cabang').val() == '')
   {
    alert('cabang tidak boleh kosong');
    return false;
  }


  $.ajax({
    type: "POST",
    url: "<?php echo base_url().'inventori/simpan_stok_masuk'; ?>",
    data: $(this).serialize(),
     beforeSend: function() {
      $('.btn-submit').prop('disabled', true);
      $('.btn-submit').removeClass('btn-success').addClass('btn-secondary').text('Loading');
    },
    success: function(data) {
       
      window.location = '<?php echo base_url().'inventori/stok_masuk' ?>';
      $('.btn-submit').prop('disabled', false);
      $('.btn-submit').removeClass('btn-secondary').addClass('btn-success').text('Save');
    }
  });
});

  //   $( ".nama_produk" ).select2({
  //   theme: "bootstrap"
  // });




      $('#formstok').on('keyup keypress', function(e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) { 
    e.preventDefault();
    return false;
  }
});

    $('.barcode').keyup(function(event){
  if (event.keyCode === 13) {
    var id = $(this).attr('key');
    var value = $(this).val();
    if(id != ''){
      $.ajax({
        type: "POST",
        url: "<?php echo base_url().'inventori/cari_produk'; ?>",
        data: {value:value},
        dataType: 'json',
        success: function(data) {
          $('.id_produk1').val(data.id);
          $('#harga'+id).val(data.harga);
          $('#satuan'+id).text(data.satuan);
          $('.nama_produk').val(data.nama);
          $('#jumlah1').focus();
        }
      });

    }else{
      $('.nama_produk').focus();
    }
  }
});



      $('.prefetch .typeahead').typeahead(null, {
        nama: 'sample_data',
        display: 'nama',
        source:sample_data,
        limit:10,
        templates:{
          suggestion:Handlebars.compile('<div class="row" style="margin-left:0px !important;margin-right:0px !important;"><div class="col-md-12">{{nama}}</div></div>')
        }
      });



      $('.typeahead').on('typeahead:selected', function (e, data) {
        if(data.type == 'produk'){
          var code = data.id;
          var id= $('.typeahead').attr('key');
          $('.id_produk').val(code);

          $.ajax({
            type: "POST",
            url: "<?php echo base_url().'kasir/cari_produk_by_id'; ?>",
            data: {code:code},
            dataType: 'json',
            success: function(data) {
              $('.id_produk1').val(data.id);
              $('#harga'+id).val(data.harga);
              $('#satuan'+id).text(data.satuan);
              $('.nama_produk').val(data.nama);
              $('.barcode').val(data.kode);
              $('#jumlah1').focus();
            }
          });
        }
      });


 


</script>