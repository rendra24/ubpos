 <style type="text/css">
   .select2-container .select2-selection--single{
    height: 35px;
    margin-top: -5px;
  }

  .select2-container--bootstrap .select2-selection {
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    background-color: #fff;
    border: 1px solid #f2f2f2;
    border-radius: 2px !important;
    color: #555;
    font-size: 14px;
    outline: 0;
    font-size: 0.75rem;
    padding: 0.56rem 0.75rem !important;
    line-height: 14px !important;
    font-weight: 300;
  }
</style>


<div class="main-panel">
  <div class="content-wrapper">
    <div class="row">


      <div class="col-12">
        <div class="card">
          <div class="card-body">



           <form method="post" class="formbarang">

            <div class="row">
              <div class="col-lg-6">
                <h4 class="card-title">Tambah Produk</h4>
              </div>
              <div class="col-lg-6" style="text-align: right;">
               <button type="submit" class="btn btn-success btn-fw btn-submit">Simpan</button>
             </div>
           </div>

           <div class="row">
            <div class="col-lg-4">
              <div class="form-group">
                <label for="exampleInputEmail1">Gambar Produk</label>
                <input type="file" class="form-control" name="files[]" multiple=""/>
              </div>
            </div>
            <div class="col-lg-8">
              <div class="form-group">
                <label for="exampleInputEmail1">Nama Produk  Indonesian<span class="text-danger" title="Reuired">*</span> </label>
                <input type="text" class="form-control" id="nama_produk" name="nama_produk" value="<?php if(isset($id)){ echo $barang['nama']; } ?>" required>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Nama Produ English <span class="text-danger" title="Reuired">*</span> </label>
                <input type="text" class="form-control" id="nama_produk_eng" name="nama_produk_eng" value="<?php if(isset($id)){ echo $barang['nama_eng']; } ?>" required>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Kategori <span class="text-danger" title="Reuired">*</span></label>
                <select class="form-control" id="id_kategori" name="id_kategori" required>
                 <?php
                 $get= $this->db->get_where('kategori', array('status' => 1))->result_array();
                 foreach ($get as $row) {
                  ?>
                  <option value="<?php echo $row['id_kategori'] ?>" <?php if(isset($id)){ if($row['id_kategori'] == $barang['id_kategori']){  echo "selected"; } } ?>><?php echo $row['nama_kategori'] ?></option>
                  <?php
                }
                ?>

              </select>
            </div>
          </div>
          <div class="col-lg-6">

            <?php if(isset($id)){
              ?>
              <input type="hidden" name="id_barang" value="<?php echo $id; ?>">
              <?php
            } ?>


            <div class="form-group">
              <label for="exampleInputEmail1">Satuan <span class="text-danger" title="Reuired">*</span></label>
              <select class="form-control" id="satuan" name="satuan" required>
                <?php foreach ($unit as $row) { ?>
                  <option value="<?php echo $row['id'] ?>" <?php if(isset($id)){ if($row['id'] == $barang['satuan']){  echo "selected"; } } ?>><?php echo $row['nama_satuan']; ?></option>
               <?php } ?>
              </select>
            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">Supplier <span class="text-danger" title="Reuired">*</span></label>

              <select class="form-control col-lg-12 supplier" name="supplier" required>
                <option value="">-- Supplier --</option>
                <?php 
                $get = $this->db->get('supplier')->result_array();
                foreach ($get as $row) {
                  ?>
                  <option value="<?php echo $row['id']; ?>"><?php echo $row['nama'] ?></option>
                  <?php
                }
                ?>
              </select>
            </div>

            <div class="form-group">
              <label>Keterangan</label>
              <textarea name="keterangan" id="keterangan" class="form-control"><?php if(isset($id)){ echo $barang['ket']; } ?></textarea>
            </div>
          </div>


          <div class="col-lg-6">

           <div class="form-group">
            <label>Stock Alert</label>
            <input type="number" name="stok_alert" class="form-control" value="<?php if(isset($id)){ echo $barang['stok_alert']; } ?>">
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Barcode <span class="text-danger" title="Reuired">*</span></label>
            <input type="text" class="form-control" id="barcode" name="barcode" placeholder="" value="<?php if(isset($id)){ echo $barang['kode']; } ?>" required>
          </div>

          <div class="form-group">
            <label>Status Display <span class="text-danger" title="Reuired">*</span></label><br>
            <input type="checkbox" name="status" data-toggle="toggle" data-on="Active" data-off="Inactive" id="toggle-status" data-onstyle="success">
          </div>

        </div>

        <div class="col-lg-12"><hr></div>

        <div class="col-lg-6">


          <div class="form-group">
            <label>Harga Beli <span class="text-danger" title="Reuired">*</span></label>
            <input type="number" min="0" name="harga_beli" class="form-control" value="<?php if(isset($id)){ echo $barang['harga_beli']; } ?>" required>
          </div>


          <div class="form-group">
            <label>Harga Beli Marketing<span class="text-danger" title="Reuired">*</span></label>
            <input type="number" min="0" name="harga_marketing" class="form-control" value="<?php if(isset($id)){ echo $barang['harga_marketing']; } ?>" required>
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Harga (1) <span class="text-danger" title="Reuired">*</span></label>
            <input type="number" min="0" class="form-control" id="harga" name="harga1"  value="<?php if(isset($id)){ echo $barang['harga']; } ?>" required>
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Harga (3) <span class="text-danger" title="Reuired">*</span></label>
            <input type="number" min="0" class="form-control" id="harga" name="harga3" value="<?php if(isset($id)){ echo $barang['harga_3_satuan']; } ?>" required>
          </div>

        </div>

        <div class="col-lg-6">

          <div class="form-group">
            <label for="exampleInputEmail1">Harga (6) <span class="text-danger" title="Reuired">*</span></label>
            <input type="number" min="0" class="form-control" id="harga" name="harga6"  value="<?php if(isset($id)){ echo $barang['harga_6_satuan']; } ?>" required>
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Harga (12) <span class="text-danger" title="Reuired">*</span></label>
            <input type="number" min="0" class="form-control" id="harga" name="harga12"  value="<?php if(isset($id)){ echo $barang['harga_12_satuan']; } ?>" required>
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Harga (24) <span class="text-danger" title="Reuired">*</span></label>
            <input type="number" min="0" class="form-control" id="harga" name="harga24"  value="<?php if(isset($id)){ echo $barang['harga_24_satuan']; } ?>" required> 
          </div>




        </div>
      </div>
             <!--  <button type="submit" class="btn btn-success mr-2">Submit</button>
              <button class="btn btn-light">Cancel</button> -->
            </form>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>



<script type="text/javascript">


$(document).ready(function(){

  $(".supplier").select2({
  theme: "bootstrap"
});

   <?php if(isset($id)){ 
  if($barang['status'] == 1){
    ?>
    $('#toggle-status').bootstrapToggle('on');
    <?php
  }

  if(isset($barang['id_supplier'])){
    ?>
    $('.supplier').select2('val', ['<?php echo $barang['id_supplier']; ?>']);
    <?php
  }
} 
?>
});








$(".formbarang").submit(function(event){
  event.preventDefault();

  var formData = new FormData(this);

  $.ajax({
    type: "POST",
    url: "<?php echo base_url().'admin/simpan_barang'; ?>",
    data:formData,
    processData:false,
    contentType:false,
    cache:false,
    async:false,
    beforeSend: function() {
      $('.btn-submit').prop('disabled', true);
      $('.btn-submit').removeClass('btn-success').addClass('btn-secondary').text('Loading');
    },
    success: function(data) {

      if(data == 1)
      {
        window.location = '<?php echo base_url().'admin/barang'; ?>';
      }
    }
  });

});

</script>