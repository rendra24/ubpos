<div class="main-panel">
  <div class="content-wrapper">
    <div class="row">


      <div class="col-12">
        <div class="card">
          <div class="card-body">

            <div class="basic-form">
              <form method="POST" id="formdata">

               <div class="row">
                <div class="col-lg-6">
                  <h4 class="card-title">Add Category</h4>
                </div>
                <div class="col-lg-6" style="text-align: right;">
                 <button type="submit" class="btn btn-success btn-fw btn-submit">Save</button>
               </div>
             </div>

             <div class="row">
              <div class="col-md-6">                                                               

               <div class="form-group">
                <label>Nama Kategori <span class="text-danger" title="Reuired">*</span></label>
                <input name="nama_kategori" type="text" class="form-control input-default " placeholder="Nama Kategori" value="<?php if(isset($id)){ echo $kategori['nama_kategori']; } ?>" required>

                <?php if(isset($id)){ ?>
                  <input type="hidden" name="id_kategori" value="<?php if(isset($id)){ echo $kategori['id_kategori']; } ?>">
                <?php } ?>
              </div>

              <div class="form-group">
                <label>Status Display <span class="text-danger" title="Reuired">*</span></label><br>
                <input type="checkbox" name="status" data-toggle="toggle" data-on="Active" data-off="Inactive" id="toggle-status" data-onstyle="success">
              </div>

            </div>


          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>

<script type="text/javascript">

 <?php if(isset($id)){ 
  if($kategori['status'] == 1){
    ?>
    $('#toggle-status').bootstrapToggle('on');
    <?php
  }
}
?>



$('#formdata').submit(function(event){
  event.preventDefault();
  var formdata = $(this).serialize();

  $.ajax({
    type: "POST",
    url: "<?php echo base_url().'admin/simpan_kategori'; ?>",
    data: formdata,
    beforeSend: function() {
      $('.btn-submit').prop('disabled', true);
      $('.btn-submit').removeClass('btn-success').addClass('btn-secondary').text('Loading');
    },
    success: function(data){

      if(data == 1)
      {

        window.location = "<?php echo base_url().'admin/kategori'; ?>";

      }

    }
  });

});
</script>