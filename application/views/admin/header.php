<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>UPOS Admin Panel</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url().'admin_assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css'; ?>">
  <link rel="stylesheet" href="<?php echo base_url().'admin_assets/vendors/css/vendor.bundle.base.css'; ?>">
  <link rel="stylesheet" href="<?php echo base_url().'admin_assets/vendors/css/vendor.bundle.addons.css'; ?>">
  <link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url().'admin_assets/css/style.css'; ?>">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo base_url().'favicon.ico'; ?>" />
  <!-- <script type="text/javascript" src="<?php //echo base_url().'admin_assets/js/jquery.min.js'; ?>"></script> -->
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>

  <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>

  <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>

  <script type="text/javascript" src="<?php echo base_url('datatables/datatables.min.js') ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('datatables/lib/js/dataTables.bootstrap.min.js') ?>"></script>

  <style type="text/css">
    .select2-container--default.select2-container--focus .select2-selection--multiple
    {
      border-color: #f2f2f2 !important;
    }

    .select2-container--default .select2-selection--multiple
    {
      border-color: #f2f2f2 !important;
    }
  </style>
</head>
<?php 

$get_order = $this->db->query('SELECT * FROM penjualan WHERE status_penjualan=1 AND status !=5 OR status_penjualan=1 AND status =1')->result_array();
foreach ($get_order as $row) {
  $timestamp = strtotime($row['tgl']) + 60*60*12;

  $time = date('Y-m-d H:i:s', $timestamp);
  
  if(date('Y-m-d H:i:s') >= $time){
    $data['status'] = 5;
    $this->db->update('penjualan',$data, array('id' => $row['id']));
  }
}

?>
<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="index.html">
          <img src="<?php echo base_url().'assets/logo.svg'; ?>" alt="logo" style="height: auto;" />
        </a>
        <a class="navbar-brand brand-logo-mini" href="index.html">
          <img src="images/logo-mini.svg" alt="logo" />
        </a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
       <!--  <ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex">
          <li class="nav-item">
            <a href="#" class="nav-link">Schedule
              <span class="badge badge-primary ml-1">New</span>
            </a>
          </li>
          <li class="nav-item active">
            <a href="#" class="nav-link">
              <i class="mdi mdi-elevation-rise"></i>Reports</a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="mdi mdi-bookmark-plus-outline"></i>Score</a>
          </li>
        </ul> -->
        <?php 
        $produk_stok = $this->db->query("SELECT b.nama , (SUM(a.masuk) - SUM(a.keluar)) as sisa_stok , b.stok_alert FROM produk_transaksi a 
          LEFT JOIN produk b ON a.id_produk = b.id
          GROUP BY a.id_produk");


        $total_stok_alert = 0;
        foreach ($produk_stok->result_array() as $row) {
          if($row['sisa_stok'] <= $row['stok_alert']){
            $total_stok_alert += 1;
          }
        }
        ?>
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" id="messageDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <i class="mdi mdi-file-document-box"></i>
              <?php if($total_stok_alert > 0){ ?>
                <span class="count"><?php echo $total_stok_alert; ?></span>
              <?php } ?>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="messageDropdown">
              <div class="dropdown-item" onclick="window.location='<?php echo base_url().'laporan/stok'; ?>';" style="cursor: pointer;">
                <p class="mb-0 font-weight-normal float-left">You have <?php echo $total_stok_alert; ?> product min stock
                </p>
                <span class="badge badge-info badge-pill float-right">View all</span>
              </div>
              <div class="dropdown-divider"></div>
              <?php 

              if($total_stok_alert > 0){
                $produk_stok = $produk_stok->result_array();
                $i= 1;
                foreach ($produk_stok as $row) {
                 if($row['sisa_stok'] <= $row['stok_alert']){
                  ?>
                  <a class="dropdown-item preview-item">
                  <!-- <div class="preview-thumbnail">
                    <img src="images/faces/face4.jpg" alt="image" class="profile-pic">
                  </div> -->
                  <div class="preview-item-content flex-grow">
                    <h6 class="preview-subject ellipsis font-weight-medium text-dark"><?php echo $row['nama']; ?>
                    <!-- <span class="float-right font-weight-light small-text">1 Minutes ago</span> -->
                  </h6>
                  <p class="font-weight-light small-text">
                    <?php echo $row['sisa_stok']; ?> Stock
                  </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <?php 
              if($i == 10){
                break;
              }
              $i++;
            }
          } 
        }
        ?>

      </div>
    </li>
         <!--  <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
              <i class="mdi mdi-bell"></i>
              <span class="count">4</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
              <a class="dropdown-item">
                <p class="mb-0 font-weight-normal float-left">You have 4 new notifications
                </p>
                <span class="badge badge-pill badge-warning float-right">View all</span>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-success">
                    <i class="mdi mdi-alert-circle-outline mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-medium text-dark">Application Error</h6>
                  <p class="font-weight-light small-text">
                    Just now
                  </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-warning">
                    <i class="mdi mdi-comment-text-outline mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-medium text-dark">Settings</h6>
                  <p class="font-weight-light small-text">
                    Private message
                  </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-info">
                    <i class="mdi mdi-email-outline mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-medium text-dark">New user registration</h6>
                  <p class="font-weight-light small-text">
                    2 days ago
                  </p>
                </div>
              </a>
            </div>
          </li> -->
          <li class="nav-item dropdown d-none d-xl-inline-block">
           <?php 
           $setting = $this->db->get('setting')->row_array();
           ?>
           <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
            <span class="profile-text"><?php echo $_SESSION['nama']; ?></span>
          </a>
          <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">

            <!-- <a class="dropdown-item mt-2" href="<?php //echo base_url().'pengaturan/akun'; ?>">
              Manage Accounts
            </a> -->
            <a class="dropdown-item" href="<?php echo base_url().'masuk/keluar'; ?>">
              Sign Out
            </a>
          </div>
        </li>
      </ul>
      <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
        <span class="mdi mdi-menu"></span>
      </button>
    </div>
  </nav>
  <!-- partial -->
  <div class="container-fluid page-body-wrapper">
    <!-- partial:partials/_sidebar.html -->
    <nav class="sidebar sidebar-offcanvas" id="sidebar">
      <ul class="nav">
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url().'admin'; ?>">
            <i class="menu-icon mdi mdi-television"></i>
            <span class="menu-title">Dashboard</span>
          </a>
        </li>

        <?php if($_SESSION['level'] == 1){ ?>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon mdi mdi-file-document-box"></i>
              <span class="menu-title">Laporan</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url().'laporan/transaksi_penjualan' ?>">Transaksi Penjualan</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url().'laporan/stok' ?>">Stok</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url().'laporan/kas' ?>">Kas</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url().'laporan/laba_produk' ?>">Laba Produk</a>
                </li>
              </ul>
            </div>
          </li>

          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-prd" aria-expanded="false" aria-controls="ui-int">
              <i class="menu-icon mdi mdi-shopping"></i>
              <span class="menu-title">Produk</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-prd">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url().'admin/barang' ?>">Data Produk</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url().'admin/kategori' ?>">Data Kategori</a>
                </li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-int" aria-expanded="false" aria-controls="ui-int">
              <i class="menu-icon mdi mdi-content-copy"></i>
              <span class="menu-title">Inventaris</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-int">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url().'inventori/kartu_stok' ?>">Kartu Stok</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url().'inventori/stok_masuk' ?>">Stok Masuk</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url().'inventori/stok_keluar' ?>">Stok Keluar</a>
                </li>
                <!-- <li class="nav-item">
                  <a class="nav-link" href="pages/ui-features/typography.html">Transfer Stock</a>
                </li> -->
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url().'inventori/opname' ?>">Stok Opname</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url().'inventori/supplier' ?>">Supplier</a>
                </li>
              </ul>
            </div>
          </li>

         <!--  <li class="nav-item">
            <a class="nav-link" href="<?php //echo base_url().'pengaturan/cabang';?>">
              <i class="menu-icon mdi mdi-store"></i>
              <span class="menu-title">Outlet</span>
            </a>
          </li> -->
         <!--  <li class="nav-item">
            <a class="nav-link" href="<?php //echo base_url().'pengaturan/pegawai';?>">
              <i class="menu-icon mdi mdi-account-box"></i>
              <span class="menu-title">Employees</span>
            </a>
          </li> -->
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url().'pengaturan/users';?>">
              <i class="menu-icon mdi mdi-account-box"></i>
              <span class="menu-title">Users/Pengguna</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url().'pengaturan/pelanggan';?>">
             <i class="menu-icon mdi mdi-account-multiple"></i>
             <span class="menu-title">Pelanggan</span>
           </a>
         </li>


         <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url().'kasir/orders';?>">
           <i class="menu-icon mdi mdi-receipt"></i>
           <span class="menu-title">Order Online</span>
         </a>
       </li>
       <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url().'kasir';?>">
         <i class="menu-icon mdi mdi-receipt"></i>
         <span class="menu-title">Kasir Penjualan</span>
       </a>
     </li>
     <li class="nav-item">
      <a class="nav-link" href="<?php echo base_url().'pengaturan/akun';?>">
       <i class="menu-icon mdi mdi-settings"></i>
       <span class="menu-title">Pengaturan</span>
     </a>
   </li>

 <?php } ?>

 <?php if($_SESSION['level'] == 2){ ?>
  <li class="nav-item">
    <a class="nav-link" href="<?php echo base_url().'pengaturan/pelanggan';?>">
     <i class="menu-icon mdi mdi-account-multiple"></i>
     <span class="menu-title">Customers</span>
   </a>
 </li>
 <li class="nav-item">
  <a class="nav-link" href="<?php echo base_url().'kasir';?>">
   <i class="menu-icon mdi mdi-receipt"></i>
   <span class="menu-title">Point Of Sale</span>
 </a>
</li>
<?php } ?>

<?php if($_SESSION['level'] == 3){ ?>
 <li class="nav-item">
  <a class="nav-link" href="<?php echo base_url().'marketing/barang';?>">
   <i class="menu-icon mdi mdi-shopping"></i>
   <span class="menu-title">Produk</span>
 </a>
</li>
<li class="nav-item">
  <a class="nav-link" href="<?php echo base_url().'laporan/transaksi_penjualan' ?>">
   <i class="menu-icon mdi mdi-file-document-box"></i>
   <span class="menu-title">Laporan</span>
 </a>
</li>
<li class="nav-item">
  <a class="nav-link" href="<?php echo base_url().'pengaturan/pelanggan';?>">
   <i class="menu-icon mdi mdi-account-multiple"></i>
   <span class="menu-title">Pelanggan</span>
 </a>
</li>

<li class="nav-item">
  <a class="nav-link" data-toggle="collapse" href="#ui-prd" aria-expanded="false" aria-controls="ui-int">
    <i class="menu-icon mdi mdi-receipt"></i>
    <span class="menu-title">Online Sale</span>
    <i class="menu-arrow"></i>
  </a>
  <div class="collapse" id="ui-prd">
    <ul class="nav flex-column sub-menu">
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url().'penjualan/orders' ?>">Data Orders</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url().'penjualan' ?>">New Order</a>
      </li>
    </ul>
  </div>
</li>
<?php } ?> 

</ul>
</nav>