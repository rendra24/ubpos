
<div class="page-wrapper">

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Pelanggan</h3> </div>

        </div>
        
        <div class="container-fluid">
           
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-title">

                        </div>
                        <div class="card-body">
                            <div class="basic-form">
                                <form method="POST" id="formdata">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Nama</label>
                                                <input type="text" name="nama" class="form-control input-default " placeholder="Nama" value="<?php if(isset($id)){ echo $user['nama']; } ?>">

                                                <?php if(isset($id)){ ?>
                                                    <input type="hidden" name="id" value="<?php if(isset($id)){ echo $user['id']; } ?>">
                                                <?php } ?>
                                            </div>

                                            <div class="form-group">
                                                <label>Jenis kelamin</label>
                                                <select class="form-control input-default" name="jenis_kelamin">
                                                    <option>-- Pilih Kelamin --</option>
                                                    <option value="L" <?php if(isset($id)){ if($user['kelamin'] == 'L'){ echo 'selected'; } } ?>>Laki-Laki</option>
                                                    <option value="P" <?php if(isset($id)){ if($user['kelamin'] == 'P'){ echo 'selected'; } } ?>>Perempuan</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label>Alamat</label>
                                                <input type="text" name="alamat" class="form-control input-default " placeholder="Alamat" value="<?php if(isset($id)){ echo $user['alamat']; } ?>">
                                            </div>

                                            <div class="form-group">
                                                <label>Kota</label>
                                                <input type="text" name="kota" class="form-control input-default " placeholder="Kota" value="<?php if(isset($id)){ echo $user['kota']; } ?>">
                                            </div>


                                            <div class="form-group">
                                                <label>Provinsi</label>
                                                <input type="text" name="provinsi" class="form-control input-default " placeholder="Provinsi" value="<?php if(isset($id)){ echo $user['propinsi']; } ?>">
                                            </div>

                                            <div class="form-group">
                                                <label>Telepone</label>
                                                <input type="text" name="tlp" class="form-control input-default " placeholder="Telepone" value="<?php if(isset($id)){ echo $user['tlp']; } ?>">
                                            </div>




                                        </div>

                                        <div class="col-md-6">
                                           <div class="form-group">
                                            <label>Kode Pos</label>
                                            <input type="text" name="kode_pos" class="form-control input-default " placeholder="Kode Pos" value="<?php if(isset($id)){ echo $user['kode_pos']; } ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="text" name="email" class="form-control input-default " placeholder="Email" value="<?php if(isset($id)){ echo $user['email']; } ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Password</label>
                                            <input type="password" name="password" class="form-control input-default " placeholder="Password">
                                        </div>

                                        
                                        <div class="form-group">
                                            <label>Cabang</label>
                                            <select class="form-control input-default" name="id_cabang">
                                                <option>-- Pilih Cabang --</option>
                                                <?php foreach ($cabang as $row) { ?>
                                                   <option value="<?php echo $row['id_cabang'] ?>" <?php if(isset($id)){ if($user['id_cabang'] == $row['id_cabang']){ echo 'selected'; } } ?>><?php echo $row['nama_cabang'] ?></option> 
                                               <?php } ?>
                                           </select>
                                       </div>



                                       <div class="form-group">
                                        <label>Limit Order</label>
                                        <input type="text" name="limit_order" class="form-control input-default " placeholder="Limit Order" value="<?php if(isset($id)){ echo $user['limit_order']; } ?>">
                                    </div>

                                </div>


                            </div>

                            <div class="col-md-12">
                                <button type="submit" class="btn btn-info col-md-2">Save</button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>

<script type="text/javascript">
    $('#formdata').submit(function(event){
        event.preventDefault();
        var formdata = $(this).serialize();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url().'user/simpan_user'; ?>",
            data: formdata,
            success: function(data){

                if(data == 1)
                {
                   swal("Success!","Berhasil menambahkan pelanggan.", "success")
                   .then((value) => {
                      window.location = "<?php echo base_url().'user/pelanggan'; ?>";
                  });
               }

           }
       });

    });
</script>